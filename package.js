
Package.describe({
  name: 'vh:survey',
  summary: 'survey package for Varsa Health',
  version: '1.0.0',
  git: ' /* Fill me in! */ '
});

Package.onUse(function(api) {
  api.versionsFrom('1.0.3.1');
  api.use(['vh:app-base@0.0.0',
         'vh:user@0.0.0',
         'templating',
         'browser-policy',
         'momentjs:moment@2.9.0',
        // 'mrt:twilio-meteor@1.1.0',
         'percolate:synced-cron'
        // 'clinical:nvd3@0.0.3',
        // 'd3js:d3@3.4.13'
         ]);
  api.addFiles('src/surveyModel.js');
  api.addFiles(['src/client/templates/survey.html',
                'src/client/templates/surveyManager.html',
                'src/client/templates/autoform.html',
                'src/client/templates/graphs.html',
		'src/client/templates/modals.html',
                'src/client/templates/surveyInvite.html',
                'src/client/surveys.css'],
                'client');
  api.addFiles(['src/client/clientStartup.js',
                'src/client/surveyTakeClient.js',
		            'src/client/surveyInvite.js',
                'src/client/surveyClient.js','src/client/graphs.js',
                'src/client/routes.js'],
                'client');
  api.addFiles(['src/server/surveyServer.js','src/server/surveyPublications.js','src/server/surveyMethods.js'],'server');  
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('survey');
  api.addFiles('survey-tests.js');
});
