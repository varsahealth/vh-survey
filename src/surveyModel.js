/*
	survey package
	Uses simpleSchema to define behavior for autoform -
	Most code here is for co-relating records in one to many relationships to itself.
	Survey {
		title,
		owner : <Meteor.userId()>,
		domain : <Name of organization/role?>,
		category : <TBD>,
		banks : [
			{title,answerId}
		],
		questions :[
			{bankId: <ABOVE title>,q: Question}
		]
	}
*/



var dMin = 0;
var dMax = 20;

if(typeof VH.schemas == "undefined"){
	VH.schemas = {};
}
if(typeof VH.SurveyRules == "undefined"){
	VH.SurveyRules = {};
	VH.SurveyRules.default = {
		insert: function (userId, doc) {
			if(userId == doc.owner ){
				return true;
			}
			return false;
		},
		update: function (userId, doc, fields, modifier) {
			if(userId == doc.owner || VH.Survey.isAdmin(doc.organization,userId) ){
				return true;
			}
			return false;
		},
		remove: function (userId, doc) {
			if(userId == doc.owner){
				return true;
			}
			return false;
		}
	};
	// allowed default
	VH.SurveyRules.anonymousUpdate =
	{
		insert: function (userId, doc) {
			if(userId == doc.owner){
				return true;
			}
			return false;
		},
		update: function (userId, doc, fields, modifier) {
			/*
			keeping this simple...
			if(userId == doc.owner ){
				return true;
			}
			return false;
			*/
			console.log('attempting to update...');
			return true;
		},
		remove: function (userId, doc) {
			if(userId == doc.owner){
				return true;
			}
			return false;
		}
	};

}
/*
 *	Survey Mongo Collections
 */ 

VH.SurveyActivity = new Meteor.Collection("vh_survey_activity");

VH.Survey = new Meteor.Collection('vh_survey');

VH.SurveyAnswers = new Meteor.Collection('vh_survey_answers');

// will need to switch out these rules for the survey admin somehow...
VH.SurveyResponses = new Meteor.Collection("vh_survey_responses");

VH.SurveyCalc = new Meteor.Collection("vh_survey_calc");

/*
 *	Survey Schemas (SimpleSchema)
 */ 

VH.schemas.vh_survey_responses = new SimpleSchema({
	owner :{
		type:String,
		label:"Owner",
		optional:true
	},
	timestamp :{
		type:Date,
		label:"Time Entered",
		autoValue : function(){
			return new Date;
		}
	},
	surveyId :{
		type : String,
		label : "Survey Id"
	},
	// this will hurt ... ahhh
	surveyResponse:{
		type:[String],
		label:"Response"
	}
});

VH.schemas.vh_survey = new SimpleSchema({
	title :{
		type:String,
		label:"Title",
		autoform:{
			afFieldInput:{
				class: "surveyTextInput"
			}
		}
	},
	// define wether the template is available for organization providers to use
	// as a starter, probably enforce some editing rules to only permit owner
	// to edit
	templateType :{
		type:String,
		label:"Template Type",
		optional:true,
		autoform:{
			type:"select-radio-inline",
			options:function(){
				// not sure if 'none' will remove...
				if( Session.equals("surveyTemplate",undefined) && VH.Survey.isSuperAdmin() ){
					return [{label:"None",value:""},
				        {label:"Organization",value:"org"},
				        {label:"Global",value:"all"}];
				}else{
					// only permit admin or superadmin to create global templates based on other templates
					return [{label:"None",value:""},
				        {label:"Organization",value:"org"}
				        ];
				}
			}
		}
	},
	owner :{
		type:String,
		label:"Owner",
		autoValue : function(){
			if(Meteor.isServer){
				if(typeof this.value != "undefined" && this.value != '')
				return this.value;
			}
			var uId = Meteor.userId()
			if(!uId){
				if(typeof this.userId != "undefined"){
					return this.userId;
				}
				alert("Not Logged in.")
				return false;
			}else{
				return uId;
			}
		}
	},
	organization:{
		type:String,
		label:"Organizational Domain",
		optional:true
		// value set on clientside insert
	},
	category:{
		type:String,
		label:"Category",
		optional:true
	},
	userType :{
		type:String,
		label: "Type of user",
		autoform:{
			type:"select-radio-inline",
			options:function(){
				return [{label:"Normal",value:"user"},
				        {label:"Circle of Care",value:"circle"},
				        {label:"Other",value:"other"}];
			}
		}
	},
	issueDate : {
		type: Date,
		label:"Issue Date",
		autoValue : function(){
			return new Date;
		},
		optional:true
	},
	issueInterval :{
		type:String,
		label: "Retake Interval",
		autoform:{
			type:"select-radio-inline",
			options:function(){
				return [
				        {label:"Weekly",value:"weekly"},
				        {label:"Bi-Weekly",value:"biweekly"},
				        {label:"Daily",value:"daily"},
				        {label:"Monthly",value:"monthly"}];
			}
		},
		optional:true
	},
	questions:{
		type:[Object],
		label:"Questions",
		optional:true	
	},
	"questions.$.q" : {
		type: String,
		label: "Question",
		optional:true,
		autoform:{
			afFieldInput : {
				type:"textarea",
				rows : 3,
			
				class : "surveyTextInput"
			}
		}
	},
	"questions.$.answerId":{
		type:String,
		label:"answer",
		optional : true,
		autoform:{
			type: "select",
			afFieldInput:{
				class : "customSelectField"
			},
			options: function(){
				var r = []
				VH.SurveyAnswers.find().map(function (obj) {
					r.push({label : obj.title,value: obj._id})
					// ...
				});
				if(r.length > 0){
					return r;
				}
				else{
					return [];
				}
			}
		}
	},
	"questions.$.order" :{
		type: Number,
		label: "Order in bank",
		optional:true
		
		// maybe write some fancy pants stuff for auto filling this... onBeforeSubmit?
	},
	"questions.$.labelStyle":{
		type:String,

		autoform :{
			type: "select",
			options: function(){
				 return [
				 	{label:"Full Row",value:"row"},
				 	{label:"Left", value:"left"},
				 	{label:"Right",value:"right"}
				 	];
			}
		},
		optional:true
	},
	// this value never gets entered.... some kind of weird mongo schema bug
	"questions.$.isPrompt" :{
		type: String,
		label: "Prompt Only",
		autoform:{
			type:"select",
			options: function(){
				return [{label:"No",value:'false'},
							{label:"Yes",value:'true'}]
			}
		},
		optional: true
	}
});

VH.schemas.vh_answer = new SimpleSchema({
	title:{
		type:String,
		label:"Title",
		optional : true
	},
	owner:{
		type:String,
		label:"Owner"
		// no more autovalue ... only on insert...
	},
	mSize:{
		type:Number,
		label:"Total answers",
		min: dMin,
		max: dMax
	},
	aType:{
		type:String,
		label:"answer Type",
		autoform:{
				type: "select",
				options: function(){
					// get available options..
					//var Session.get("selectedanswerId");
					//return answers.find();
					// mongoid becomes key value for the value of the title of the record
					/*
					These options are used for the rendering of each answer per each question bank.
					scale - provides a numerical scale to each question from bank that designates 
					the answer (as answerId). Returns a number.

					tag - provides each question in the bank of 'tags' or answers to select.
					weightedBank - provides a bank of answers for a specific question for the
					user to rate on the defined scale in multiple areas including 'Other'. Returns 
					a string. The use of the value in this context is optional. (I.e. tags can be
					given a value to help in formulating scores if needed if.)

					 Use for questions such as
					"what degree you felt your anexity has affected the following areas of 
					your life
					"
					weightedBankTag/s - similar to above except the user can select 
					each bank that they provide the scaled response to : either multiple areas 
					or a single one, returns an or an array of objects { answerTitle : answerValue }

					*/
					return [{label:"Number Scale",value:"scale"},
							{label:"Single Selection Value",value:"tag"},
							{label:"Multiple Selection Values",value:"tags"},

							{label:"Bank of Weighted Values ",value:"wBank"},
							{label:"Bank of Weighted Multiple Selection Values",value:"wBankTags"},
							{label:"Bank of Weighted Selection Values",value:"wBankTag"}];
				}
			}			
	},
	isRadio : {
		// should be a Boolean but pages has a weird checkbox selection
		// thing going on that doesn't let me select checkboxes...
		type:Boolean,
		label : "Radio Selection",
		optional: true,
		autoform : {
			type:"boolean-checkbox"
			
		}
	},
	mFirst:{
		type:Number,
		label:"First answer",
		min: dMin,
		max: dMax
	},
	mLast:{
		type:Number,
		label:"Last answer",
		min: dMin,
		max: dMax
	},
	aValues:{
		type:[Object],
		label:"answers",
		optional:true
	},
	"aValues.$.valueLabel" : {
		type:String,
		label: "Label for answer",
		// autofil this order value...
	},
	"aValues.$.valueOrder" : {
		type:Number,
		label : "answer order",
		optional:true
		// value filled in on update...
		// wont need this value for aType value / tags
		// also needs to be unique ?
		
	},
	"aValues.$.valueClass" : {
		type:String,
		label : "Assign a class to the label",
		optional : true
	}
});

VH.schemas.vh_survey_calc = new SimpleSchema({
	// hidden field ... i
	"owner" :{
		type:String,
		optional:true
	},
	"organization" : {
		type:String,
		label:"Org",
		optional:true
	},
	"surveyId" : {
		type:String,
		label:"Survey id",
		optional: false,
		autoform:{
			type: "select",
			afFieldInput:{
				class : "customSelectField"
			},
			options: function(){
				var r = []
				VH.Survey.find({},{"sort":{owner:1}}).map(function (obj) {
					r.push({label :  VH.Survey.profileName(obj.owner) + ' - ' + obj.title ,value: obj._id});
					// ...
				});
				if(r.length > 0){
					return r;
				}
				else{
					return [];
				}
			}
		}
	},
	action:{
		type:[Object],
		label:"Action to perform",
		optional:true
	},
	"action.$.type" : {
		type:String,
		label:"Type",
		autoform:{
				type: "select",
				options: function(){
							return [
									{label:"Email",value:"email"},
									{label:"Notification",value:"notification"},
									{label:"Both",value:"email_notification"}
								    ];
				}
		}
	},
	"action.$.receiver" : {
		type:String,
		label:"Person who action is performed upon",
		autoform:{
				type: "select",
				options: function(){
							return [
									{label:"Survey Participant",value:"patient"},
									{label:"Patient Circle of care members",value:"circle"},
									{label:"Survey Provider",value:"provider"},
									{label:"Survey Administrator",value:"admin"}
								    ];
				}
		}
	},
	"action.$.calcType" : {
		type:String,
		label:"Calculation to perform",
		optional : false,
		autoform:{
				type: "select",
				options: function(){
							return [
									{label:"sum",value:"sum"},
									{label:"Aggregate sum",value:"aggsum"}
								   ];
				}
		}
	},
	"action.$.condition" : {
		type:String,
		label:"Condition",
		optional: false,
		autoform:{
				type: "select",
				options: function(){
							return [
									{label:">",value:"gt"},
									{label:"<",value:"lt"},
									{label:"=",value:"et"},
									{label:">=",value:"gte"},
									{label:"<=",value:"lte"},
									{label:"!=",value:"ne"}
								    ];
				}
		}
	},
	"action.$.condition_value" : {
		type:Number,
		label:"value",
		optional:false
	},
	"surveyQuestions" : {
		type:String,
		label:"Survey questions to include - comma separated",
		optional:true
	},
	interpretation:{
		type:[Object],
		label:"Interpretation of values",
		optional:true
	},
	"interpretation.$.title" : {
		type:String,
		label : "Title",
		optional:false
	},
	"interpretation.$.iMin" : {
		type:String,
		label : "Minimum matching value",
		optional:false
	},
	"interpretation.$.iMax" : {
		type:String,
		label : "Maximum matching value",
		optional:false
	}
});

VH.SurveyResponses.attachSchema(VH.schemas.vh_survey_responses);

VH.Survey.attachSchema(VH.schemas.vh_survey);

VH.SurveyAnswers.attachSchema(VH.schemas.vh_answer);

VH.SurveyCalc.attachSchema(VH.schemas.vh_survey_calc);

/*
 *  Survey Rules (allow)
 */

VH.SurveyResponses.allow(VH.SurveyRules.anonymousUpdate);

VH.Survey.allow(VH.SurveyRules.default);

VH.SurveyAnswers.allow(VH.SurveyRules.anonymousUpdate);

VH.SurveyCalc.allow(VH.SurveyRules.default);

/* Helpers for role checking... depreciate */
VH.Survey.isProvider = function(org,userId){
	if(Meteor.isClient){
	   var org = VH.Organization.getUrl(), userId = Meteor.userId();
	    if(VH.User.belongs(userId, org)){
	      if(VH.User.is(userId, 'provider', org)){
	      	return org;
	      }else{
	        return false;
	      }
	    }else{
	      return false;
	    }
	}else if(typeof org != "undefined"){
		if(typeof this.userId != "undefined" && typeof userId == "undefined"){
			var userId = this.userId;
			// this doesnt seem to work...
		}
		if(typeof userId != "undefined" && userId){
			if(VH.User.belongs(userId,org)){
				if(VH.User.is(userId, 'provider', org)){
					return org;
				}else{
					// user does not have permissions
					return false;
				}
			}else{
				// user does not belong to organization
				return false;
			}
		}
		return false;
	}
};

VH.Survey.isPatient = function(org,userId){
	if(Meteor.isClient){
	   var org = VH.Organization.getUrl(), userId = Meteor.userId();
	    if(VH.User.belongs(userId, org)){
	      if(VH.User.is(userId, 'patient', org)){
	      	return org;
	      }else{
	        console.log("User does not have permissions to create a survey");
	        return false;
	      }
	    }else{
	      console.log("User does not belong to this organization. Did not insert new survey.");
	      return false;
	    }
	}else if(typeof org != "undefined"){
		if(typeof this.userId != "undefined" && typeof userId == "undefined"){
			var userId = this.userId;
			// this doesnt seem to work...
		}
		if(typeof userId != "undefined" && userId){
			if(VH.User.belongs(userId,org)){
				if(VH.User.is(userId, 'patient', org)){
					return org;
				}else{
					// user does not have permissions
					return false;
				}
			}else{
				// user does not belong to organization
				return false;
			}
		}
		return false;
	}
};

VH.Survey.isAdmin = function(org,userId){
	if(Meteor.isClient){
	   var org = VH.Organization.getUrl(), userId = Meteor.userId();
	   // check against global group?
	   if(VH.User.is(userId,'superadmin',"__global_roles__")){
	   	return true;
	   }
	    if(VH.User.belongs(userId, org)){
	      if(VH.User.is(userId, 'admin', org)){
	      	return org;
	      }else{
	        console.log("User does not have permissions to create a survey");
	        return false;
	      }
	    }else{
	      console.log("User does not belong to this organization. Did not insert new survey.");
	      return false;
	    }
	}else if(typeof org != "undefined"){
		if(typeof this.userId != "undefined" && typeof userId == "undefined"){
			var userId = this.userId;
			// this doesnt seem to work...
		}
		if(typeof userId != "undefined" && userId){
			if(VH.User.is(userId,'superadmin',org)){
				return true;
			}
			if(VH.User.belongs(userId,org)){
				if(VH.User.is(userId, 'admin', org)){
					return org;
				}else{
					// user does not have permissions
					return false;
				}
			}else{
				// user does not belong to organization
				return false;
			}
		}
		return false;
	}
};

VH.Survey.isSuperAdmin = function(userId){
	console.log(userId);
	if(Meteor.isClient){
		if(typeof userId == "undefined" && typeof this.userId){
			userId = this.userId;
		}
	}else{
		if(typeof userId == "undefined" && typeof this.userId != "undefined"){
			userId = this.userId;
		}
	}
	console.log('in superadmin check');
	console.log(userId);
	console.log(VH.User.is(userId,'superadmin',"__global_roles__"));
	return VH.User.is(userId,'superadmin',"__global_roles__");

};


VH.Survey.profileName = function(userId){ 
		var theOwner = Meteor.users.findOne({_id : userId},{fields: {profile:1} } );
        if(typeof theOwner != "undefined" && typeof theOwner.profile != "undefined" && typeof theOwner.profile.firstName != "undefined" && typeof theOwner.profile.lastName != "undefined"){
          // maybe remove this call and replace with owner mongo id...
          return theOwner.profile.lastName + ', ' + theOwner.profile.firstName;
        }else{
          // was this a 'master' record?
          return "default";
        }
};
