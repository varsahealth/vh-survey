/* To do - pass this function another function at the end ... 
to take as example data

*/

var
nvd3BulletGraph = function(title,target,ranges,measures,markers){
	if(typeof target == "undefined"){
		target = ".content #chart svg";
	}
	console.log(target);
	nv.addGraph(function() {  
	  var chart = nv.models.bulletChart();
	  d3.select(target)
	      .datum(exampleData(title,ranges,measures,markers))
	      .transition().duration(1000)
	      .call(chart);
	  return chart;
	});


var exampleData=function() {

	if(typeof markers == "undefined"){
		markers = [0];
	}
  return {
  	"title":title,		//Label the bullet chart
  		//sub-label for bullet chart
  	"ranges":ranges,	 //Minimum, mean and maximum values.
  	"measures":markers,		 //Value representing current measurement (the thick blue line in the example)
  	"markers":markers			 //Place a marker on the chart (the white triangle marker)
  };
}
};

Template.surveyResponseField.rendered = function(){
	// for order processing.. not great but keeps the answer object simpler
	// assumes that we will never change the answer order once people start taking it..
	// force the admin to make a new survey if their are current responses..
	if(typeof count != "undefined"){
		count = count+1;
	}else{
		count = 1;
	}
	
	var dat = this.data;
	// yikes how do i get index of surveyResponseField ?? parent ?
	console.log(dat);

	var q = VH.Survey.findOne({_id : Session.get('selectedSurvey' ) });
	console.log(q);

	if(count  == q.questions.length + 1){
		console.log('resetting questions length');
		count = 1;
	}

	console.log(typeof count + ' count : ' + count);
	var question = _.findWhere(q.questions,{order: dat.question.order});
	// not iterating through the appropriate length of value for when prompts exist
	console.log(question);
	if( question && typeof question.answerId != "undefined"){
		//bankId: "Test", qOrder: 2, answer: "1"
		var curAns = VH.SurveyAnswers.findOne({_id:question.answerId});
		if(typeof curAns == "undefined"  || !curAns){
			console.log('no current answer found');
			return false;
		}
		// how do i get parent template id?
		var target = '#' + this.lastNode.id +' svg';
		console.log(target);
		// check if the number of aValues is equal to mSize... then evenly distribute ?
		if(curAns.mFirst == 1 && curAns.mLast == curAns.mSize){
			var ranges = [];
		
			curAns.aValues.filter(function(aValue,index){
				// also set current value label?
				ranges.push(aValue.valueOrder);
			});
			// can convert to 'the valueLabel' if we have equal number of values to the total values
			var theAnswer = parseInt(dat.answer);
			if(theAnswer){
				var title = false;
				// co relate numerical value with a string value if it exists..
				title = _.findWhere(curAns.aValues,{valueOrder : theAnswer});

				if(title === false){
					title = '';
				}else{
					title = title.valueLabel;
				}
				nvd3BulletGraph(title,target,[curAns.mFirst,curAns.mLast],ranges,[theAnswer] );

			}else{
				console.log('answer not a number?');
			}

		}else{
			// zero index processing
		}

	}else{
		console.log("answer set missing for question order " + dat.order);
	}

};
