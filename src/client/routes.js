
Router.route('/surveys',function(){
  if(typeof Meteor.userId() == "undefined" || !Meteor.userId()){
    // not logged in...
    this.ready();
    return false;
  }
  var org = Session.get("organization");
  if(this.ready()){
    if(typeof org != "undefined" && Meteor.userId()){
     if(VH.Survey.isProvider() || VH.Survey.isAdmin()){
      // redundant?
        //if(typeof userSurveysSub == "undefined"){
         Meteor.subscribe("userSurveys",org);
        //}

        this.layout('DashboardLayout');
        this.render('survey');  
      }else if(VH.Survey.isPatient()){
        console.log('is patient');
        // make sure the patient has this...
        console.log(Meteor.user());
        Meteor.subscribe("patientSurveyData",org);
        this.layout('DashboardLayout');
        this.render('availableSurveysCards');
      }else{
        // no access allowed!
        this.render('NotFound');
      }
    }else{
      this.layout('DashboardLayout');
      //VH.Alert.error("Not logged in");
    }
  }else{
    this.render("Loading");
  }
});

Router.route('/surveyCalculations',function(){
  var org = Session.get("organization");
  if(this.ready()){
    if(typeof org != "undefined" && Meteor.userId()){
       if(VH.Survey.isProvider() || VH.Survey.isAdmin()){
        
        var selectedCalc = Session.get("selectedCalculation");
        // get surveys?
        //if(typeof userSurveysSub == "undefined"){
        userSurveysSub = Meteor.subscribe("userSurveys",org);
        //}
        surveyCalcSub = Meteor.subscribe("surveyCalc",org,(selectedCalc ? typeof selectedCalc != "undefined" && selectedCalc : false));

        this.layout('DashboardLayout');
        this.render('surveyCalculations');
       }else{
        this.render('NotFound');
       }
     }else{
      this.render('NotFound');
     }
  }else{
    this.render("Loading");
  }

});
Router.route('/misfit',function(){

    var code = location.search.split('code=');
    // check if key is already valid?
    if(Meteor.userId()){
      if(typeof code != "undefined" && code != "" && code && code[1] != "undefined" ){
        code = code[1];
        
        Meteor.subscribe("misfit",code,function(e,r){
          console.log(e);
          if(typeof r != "undefined" && r){
            console.log(r);
            Meteor.subscribe("misfitSleepData",Meteor.userId());
          }
        });

        // lookup code and insert if not exists...
        //if(VH.SurveyActivity.find({_id : code,owner:Meteor.userId() }))
      }else{
        console.log('else');
        // dont call this if we already have a appr. object..
        Meteor.call("misfitAuth",function(error,response){
          console.log(error);
          if(typeof error == "undefined" && response)
            window.location.replace(response);
          else
            alert("Problem with misfit auth");
        });
      }
       Meteor.subscribe("misfitSleepData",Meteor.userId());
       this.render("misfit");
    }
  this.layout('DashboardLayout');
  //this.render('misfit');  
});

Router.route('/completedSurvey',function(){
  this.render('completedSurvey');
});

Router.route('/surveyExpired',function(){
  this.render('NotFound');
});



Router.route('/s/:id',
  {
    action : function(){
      if(this.ready()){
        if(VH.Survey.isAdmin() || VH.Survey.isProvider()){
          Meteor.subscribe("surveyPreview",this.params.id,Session.get("organization"));
          Session.set("selectedSurvey",this.params.id);
          this.render('surveyTake');
          }else{
            // 404 not found?
            this.render('NotFound');
          }   
     
      }else{
        this.render('Loading');
      }
    }
});
/*
Router.route('/survey/:id',
  {
    action : function(){
      if(this.ready()){
       Session.set("selectedSurvey",this.params.id);
        //Router.go('/surveys');
       if(typeof org != "undefined" && Meteor.userId()){
       if(VH.Survey.isProvider() || VH.Survey.isAdmin()){
        // redundant?
         Meteor.subscribe("userSurveys",org);
          var selectedSurvey = Session.get("selectedSurvey");
          if(selectedSurvey){
            // get survey responses from elsewhere....
            responseSub = Meteor.subscribe("surveyResponses",selectedSurvey,Session.get("organization"));
            //Meteor.subscribe("inviteFromManager",selectedSurvey);
          }
          this.layout('DashboardLayout');
          this.render('survey');  
        }else if(VH.Survey.isPatient()){
          console.log('is patient');
          Meteor.subscribe("usersAvailableSurveys");
          
          this.layout('DashboardLayout');
          this.render('availableSurveysCards');
        }else{
          // no access allowed!
          this.render('NotFound');
        }
      }else{
        this.layout('DashboardLayout');
        //VH.Alert.error("Not logged in");
    }
      }else{
        this.render('Loading');
      }
    }
});
*/
// create collection called 'tokens' to keep track as to revoke/expire access
// to reuse each survey?
Router.route('/surveyAnswers',{

  action : function(){
    if(this.ready()){
        if(VH.Survey.isAdmin() || VH.Survey.isProvider()){
          Meteor.subscribe("userAnswers",Session.get("organization"));
            this.layout('DashboardLayout'); 
            this.render('surveyAnswers');
        }
    }else{
        this.render('Loading');
    }
  }

})
Router.route('/t/:surveyId/:userId',{
  action : function(){
    Meteor.subscribe("getSurvey",this.params.userId,this.params.surveyId);
    Session.set("selectedSurvey",this.params.surveyId);
    Session.set("inviteUserId",this.params.userId);
    if(this.ready()){
      // begin 15 mintute timeout?
      this.render('surveyTake');
    }else{
      this.render('Loading');
    }
  }
});

