Meteor.startup(function(){
  // clear out session object.. sorta lazy
  //for(key in Session.keys){
  //  Session.set(key,undefined);
  //}

  Session.set('selectedSurvey',undefined);
  Session.set('selectedUser',undefined);
  Session.set('selectedCalc',undefined);
  Session.set("hasDates",undefined);
  Session.set("inviteUserId",undefined);


  AutoForm.setDefaultTemplateForType('afFormGroup','varsa');
  AutoForm.setDefaultTemplateForType('afArrayField','varsa');
  AutoForm.setDefaultTemplateForType('afCheckbox','varsa');
  //afArrayField_varsa

  // handlebar helper to properly handle link clicks that do not need to render
  Blaze._allowJavascriptUrls();
  // hack for meteor nonsupport of attributes in body tag and bootstrap
  // paper compatablitly
  /* Paper theme template compatability stuff... and some helper blocks to generate
   * bootstrap structures in templates
   * incoroprate these into a paper theme template 
   *
  */


  Handlebars.registerHelper('panel_heading', function(title,desc) {
    return new Handlebars.SafeString(
        '<div class="panel panel-heading"><h2 class="page-header bold">' + title
        + '<small>' + desc + '</small></h2></div>');
  });
  // font awesome Icon helper
  Handlebars.registerHelper('iFa',function(a,b){
    return '<i class="fa fa-'+ a + '"></i>' + (typeof b != "undefined" && typeof b != "object"? '&nbsp;' + b:'');
  });
 


  UI.registerHelper('equals', function(a, b) {
    return a == b; // == intentional
  });
  UI.registerHelper('nameToColumn', function(a) {
    //  banks.0.title

    // explode
    a = a.split('.').pop().replace('Id','');
    // check to see if there's an Id .. to remove that..
    return a;
    // parse passed value into a human readable name
  //  return a;
    
     // for generating table header columns ... mostly for autoform array type automata
  });

 UI.registerHelper('isNotInnerField',function(a){
// hack to let autoform templates know when to hide a label because it already exists in the table view when editing inner object/array
  a = a.split('.');
  if(a.length > 1){
    return false;
  }
  return true;
 });
  UI.registerHelper('thisIs', function() {
    console.log(this);
    return true;
  });

  Handlebars.registerHelper('a',function(){
    return "javascript:;";
  });

  Handlebars.registerHelper('selectedSurvey',function(){
    return Session.get("selectedSurvey");
  });
  AutoForm.hooks({
    newSurveyCalc :{
      before: {
        insert: function(doc,template){
          console.log('insert new survey calc');
          var org = Session.get("organization");
          if(typeof org != "undefined" && org){
            var uId =  Meteor.userId();
            if(typeof uId != "undefined" && uId){
              doc.owner = uId;
              doc.organization = org;
              console.log('returning doc');
              console.log(doc);
              return doc;
            }else{
              VH.Alert("warning",'Not logged in.');
            }
          }else{
            VH.Alert("warning","Could not find org.");
          }
        }
      }
    },
    surveyInvite:{
      before: {
        insert: function(doc,template){
          var selectedSurvey = Session.get("selectedSurvey");
          if(typeof doc.surveyId == "undefined" && selectedSurvey){
            doc.surveyId = selectedSurvey;
          }
          //doc.dateIssue = new Date();
          return doc;
        }
      }
    },
    newSurvey:{
      before:{ 
        insert: function(doc,template){
          var org = Session.get("organization");
          if(!Session.equals("surveyTemplate",undefined)){
            // fill in new doc values...
              var surveyTemplate = VH.Survey.findOne({_id : Session.get("surveyTemplate")});
              console.log(surveyTemplate);
              if(typeof surveyTemplate.questions != "undefined" && surveyTemplate.questions.length > 0){
                doc.questions = surveyTemplate.questions;
              }
          }

          if(org){
             

            var uId = Meteor.userId();
            if(!uId){
              alert("Not Logged in.")
              return false;
            }else{
              doc.owner=uId;
              doc.organization = org;
            }
          }else{
             alert("User does not have permissions to create a survey in this organization");
             return false
          }
          return doc;

            // is user an admin?
            /*
           if(typeof doc.questions != "undefined"){
            // also set the organization field to the user roles .. ?
            var org = Session.get("organization");
            if(org){
              doc.organization = org;
              doc.questions.filter(function(a,i){
                doc.questions[i].order = i + 1;
              });

            var uId = Meteor.userId();
            if(!uId){
              alert("Not Logged in.")
            }else{
              doc.owner=uId;
            }
            }else{
               alert("User does not have permissions to create a survey in this organization");
            }

              return doc;
            }else{
              alert("No questions to store");
              return false;
            }
            */
        }
      },
      after:{
        insert:function(error,result){
          console.log(error);
          console.log(result);
      	  if(typeof error == "undefined" && typeof result != "undefined" && result){ 
               // set to new record instead of showing blank form
        		Session.set("selectedSurvey",result);

            $('.modal').modal('hide');
            // close open modal
            VH.Alert.success("Created New Survey");
            AutoForm.resetForm(self.formId);
      	  }else{
      		  console.log("Could not insert survey... are you logged in?");
      	   }
           Session.set("surveyTemplate",undefined)
	  return true;
        }
      }
    }, 
    editSurvey :{
      before :{
        update : function(doc,template){
          console.log('updating editSurvey');
          console.log(template);
        
          VH.Survey.update({_id : Session.get("selectedSurvey")},template,function(){
            VH.Alert.success("Updated Survey");
          });
          return {};
        }
      }
    },
    editSurveyAnswer :{
       onSubmit: function (insertDoc, updateDoc, currentDoc) {
        alert("submit");
      console.log(arguments);
      return false;
      },
      before:{
        update : function(doc,template){
          console.log('updating surveanswer');
          if(typeof template["$set"].aValues != "undefined"){
            template["$set"].aValues.filter(function(a,i){
              template["$set"].aValues[i].valueOrder = i + 1;
            });
          }
          VH.SurveyAnswers.update({_id : Session.get("selectedAnswer")},template,function(error,result){
              if(typeof err != "undefined"){
                VH.Alert.error(err.message);
              }
              VH.Alert.success("Answer updated");
          });
            return {};
        }
      }
    },
    surveyQuestions :{
      before:{
        update: function(doc,template){
          console.log('udapting questions');
          var bankId = Session.get("selectedBank");
          if(typeof doc["$set"] != "undefined" && typeof doc["$set"].questions != "undefined" && doc["$set"].questions.length > 0){
            // look up existing questions that aren't already in the bank ...
            /*
            var questionBankOrder = Session.get("questionBankOrder");
            if(questionBankOrder ){
              // reorder questions by changing out ...
              var reOrder = [];
              questionBankOrder.filter(function(qBank,i){
                console.log(doc["$set"].banks);
                console.log(_.where(doc["$set"].banks,{title : qBank.id}));

              })
            }else{
              console.log("no question bank order");
            }*/
           
             // look up other values in too
             var r = VH.Survey.findOne({_id : Session.get("selectedSurvey")});
            if(typeof r.questions != "undefined" && typeof r.questions.length > 0){
              // combine the arrays into doc["$set"]
              r.questions.filter(function(arr,x){
                
                doc["$set"].questions[x].order = x + 1;
              });
            }else if(typeof doc['$set'].questions != "undefined"){
              // run thru questions
              doc['$set'].questions.filter(function(arr,i){
                doc['$set'].questions[i].order = i + 1;
              });
            }
            VH.Survey.update({_id : Session.get("selectedSurvey")},doc,function(){
              console.log('update sucessful upper doc set')
            });
            return {};
          }else{
             // add bankID to template...
            if(typeof template["$set"] != "undefined" && typeof template["$set"].questions != "undefined" && template["$set"].questions.length > 0){
              template["$set"].questions.filter(function(obj,x){
                if(typeof obj.bankId == "undefined"){
                  template["$set"].questions[x].bankId = bankId;
                }
              });
            }
            // get other questions from other banks
            var r = VH.Survey.findOne({_id : Session.get("selectedSurvey")});
            // messes up...
            if(typeof template["$set"] != "undefined" && typeof r.questions != "undefined" && r.questions.length > 0){
              template["$set"].questions.filter(function(arr,x){
                template["$set"].questions[x].order = x + 1;
              });
            }
            var questionBankOrder = Session.get("questionBankOrder");
            if(questionBankOrder ){
              // reorder questions by changing out ...
              var reOrder = [];
              questionBankOrder.filter(function(qBank,i){
                var bankCheck = _.where(r.banks,{title : qBank.id});
                if(bankCheck && typeof bankCheck[0] != "undefined"){
                  // set order to order in array
                  bankCheck[0].order = i + 1;
                  reOrder.push(bankCheck[0]);
                }

              });
              console.log(reOrder);
              // reintegrate new order set...
              if(typeof reOrder != "undefined" && reOrder.length > 0 ){
                template["$set"].banks = reOrder;
              }
            }

            if(typeof template["$set"] != "undefined" && typeof template["$set"].questions != "undefined"){
              // make sure any new questions get new order..
              // this is a hack
              template["$set"].questions.filter(function(q,i){
                template["$set"].questions[i].order = i + 1;

              });
              // replace the existing questions ... not too concerned about order?
              if(typeof r.questions != "undefined"){
                r.questions.filter(function(question,questionIndex){
                  if(question.bankId !== Session.get("selectedBank")){
                    if(question.bankId && question.bankId != "false"){
                      template["$set"].questions.push(question);
                    }
                  }
                });
              }
              if(!Session.get("questionBankOrder")){
                template["$set"].banks = r.banks;  
              }else{
                console.log("re order in order... hmmm");
                Session.get("questionBankOrder").filter(function(qBank,i){
                  console.log(_.where(r.banks,{title : qBank.title}));
                });
              }
              console.log(' reorder w/o session' + template["$set"].banks);
            }
    
            // probably not updating because of 'required fields' already in current survey?
            //
            VH.Survey.update({_id : doc},template,function(err,result){
              if(typeof err != "undefined"){
                VH.Alert.error(err.message);
                return false;
              }
              VH.Alert.success('Survey Updated');
              return true;
              //window.location.replace('/survey/');
              // probably go to a different page or back ..
              // refresh
            });
            return {};
          }
          return {};
        }
      }
    }
  });
});
