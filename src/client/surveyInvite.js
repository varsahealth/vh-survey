Template.surveyInvite.helpers({
	getInvites: function () {
		// look up existing invites.. if any
		return false;
		/*
		var selectedSurvey = Session.get("selectedSurvey");
		if(selectedSurvey){
			return VH.SurveyInvite.find({surveyId : selectedSurvey});
		}else{
			return VH.SurveyInvite.find();
		}*/
	},
	// isn't this a global handlebars helper?
	selectedSurvey: function(){
		var selectedSurvey = Session.get("selectedSurvey");
		if(typeof selectedSurvey != "undefined" && selectedSurvey){
			return true;
		}else
			return selectedSurvey;
	},
	getInvite : function(){
		console.log(this);
		return this;
	},
	getResponseData : function(){
		if(typeof this.responseId != "undefined" && this.responseId){
			return VH.SurveyResponses.findOne({_id : this.responseId});
		}
		else{
			return false;
		}
	}
});
/*
Template.surveyInvite.events({
	'click .deleteInvite': function () {
		console.log(this._id);
		VH.SurveyInvite.remove({_id : this._id});
		// ...
	}
});
*/
// troublesome.. probably just store the date in the inner object on object insert to avoid all this confusion?
Template.quickForm_surveyInvite.helpers({
	getResponseData: function () {
		var responseId = this.afAutoFormContext.doc
		if(typeof this.responseId != "undefined" && this.responseId){
			return VH.SurveyResponses.findOne({_id : this.responseId});
		}
		else{
			return false;
		}
	}
});