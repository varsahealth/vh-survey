/*
 * Logic for showing clients available surveys
 */

 Template.availableSurveysCards.helpers({
  getSurveys:function(){
    return VH.Survey.find();
  },
   getInvites: function () {
     // ...
     return false;
   }
 });

/*
 *  Logic for the client side survey completion. Systematically selects input fields
 *  for insertion into SurveyResponses via questions that are generated with buildSurveyQuestion.
 *  Upon sucessful  completion (within 20 mintutes and three days after the original issue weekday) page should route to template 'completedSurvey' and
 *  update the invitation record with the response, userid and responseId in the SurveyInvite collection
 */
Template.surveyTake.events({
  'click .submitSurvey': function (evt,tmpl) {
    var survey = VH.Survey.findOne(Session.get("selectedSurvey"));
    if(survey){
      // record time things were taken?
      //var answers = {surveyId : Session.get("selectedSurvey"), owner: Meteor.userId(),answers : []};
      var answers = [];
      // pre build via question.bankId .. pluck?
      var questions = buildSurveyQuestions();
      var finalR = [];

      // checkbox processing ... fun!! build tests for this portion
      questions.filter(function(q,i){
        // use a switch statement...
          if(typeof q.isPrompt != "undefined" && q.isPrompt == 'true' || typeof q.answer == "undefined"){
            console.log('no question here...');
            // insert a blank value as a placeholder?
            answers[q.order] = false;
            return false;
          }
          // answers data... only store for questions with order 1?
          if(q.answer.aType == "tags"){
            var r = [];
            $("input:checkbox[name='"+q.order+"']:checked").each(function()
            {
                // add $(this).val() to your array
                var value = $(this).val();
                if(value == "Other" || value == "other"){
                  // get this input field
                  var otherValue = $("input[name='" + q.order + 'other'+"']");
                  if(otherValue && typeof otherValue[0] != "undefined" && otherValue[0].value != ''){
                    r.push(otherValue[0].value);
                  }
                }else{
                  r.push(value);
                }
            });
            answers[q.order] = r;
          }
          if(q.answer.aType == "wBankTags"){
            // do different processing here...
            q.answer.aValues.filter(function(z){
              if(typeof answers[q.order] == "undefined"){
                answers[q.order] = {};
              }
              // this probably needs improvement
              if(z.valueLabel == "Other"){
                var otherKey = undefined;
                // replace other keyname with that provided in the text input
                otherKey = $("input[name='"  + q.order + z.valueLabel  + "other']").val();
                if(otherKey == '' || !otherKey){
                  otherKey = undefined;
                }else{
                  otherKey = 'Other ' + otherKey; 
                }
              }
              if(typeof otherKey != "undefined"){
                answers[q.order][otherKey] = parseInt($("input[name='" + q.order + z.valueLabel + "']").val());
              }else{
                answers[q.order][z.valueLabel] = parseInt($("'" + q.order + z.valueLabel + "']").val());
              }
              ;
              //weightedInputs.push(q.bankId + q.order + z.valueLabel);
            });
          }
          if(q.answer.aType == "scale"){
              // radio button processing...
              if(typeof q.answer.isRadio != "undefined" && q.answer.isRadio){
                answers[q.order] = parseInt($("input[name='" +  q.order + "']:checked").val());
              }else{
                answers[q.order] = parseInt($("input[name='" +  q.order + "']").val());
              }
          }
      });
      // null values may occur when a item is a question, but still want to keep unanswered ones (NaN type)
      // do a 20 mintute check ? 
      return VH.SurveyResponses.insert(
        {owner: Session.get("inviteUserId"),surveyId : Session.get("selectedSurvey"),surveyResponse :      _.without(answers,null)},
        function(ez,rz){
          if(typeof ez == "undefined" && typeof rz != "undefined"){
            //VH.Alert.success("Response made. Thank you!");  
            console.log('response id - ' + rz);
            Meteor.call('notifyPatientCompleteSurvey',Session.get("inviteUserId"),Session.get("selectedSurvey"),rz, function(e,r){
              if(typeof e == "undefined"){
                console.log(r);
                if(r){
                  Router.go('/completedSurvey');
                }else{
                  console.log('problem with notification call');
                }
              }else{
                console.log('error from notifyPatientCompleteSurvey');
                console.log(e);
              }
            })
          }else{
            VH.Alert.danger("Problem with response insert.");
          }
        });
    }
  }
});
var buildSurveyQuestions = function(){
  /*
  DONT USE QUESTION MARKS IN BANK TITLES?
  */
   var survey = VH.Survey.findOne({_id : Session.get("selectedSurvey")});
   console.log(survey);
    if(survey){
      var q = [];
      var answers = VH.SurveyAnswers.find().fetch();
      var questions = survey.questions;
      console.log('here');
      _.sortBy(questions,"order").filter(function(aQuestion,bankIndex){
        // filter by sorted questions on question at a time.
        //var bankQuestions = _.sortBy(_.where(questions,{"order" : aQuestion.order}),"order").filter(function(a){
          //a.bankOrder = aQuestion.order;
          // look up answer in SurveyAnswers that is related to the bank
          var answer = _.where(answers,{_id : aQuestion.answerId});
          // avoid this lookup if 'isPrompt'
          
          // undef typecheck??
          if( (typeof aQuestion.isPrompt == "undefined" || aQuestion.isPrompt == 'false') && answer && typeof answer[0] != "undefined"){
            // give question object answer values for ease of handlebars template generation
            aQuestion.answer = answer[0];
            if(typeof aQuestion.answer.aValues != "undefined" && aQuestion.answer.aValues.length > 1){
              // if we are dealing with left or right type subtract one from the col size..
              if(typeof  aQuestion.labelStyle != "undefined" &&  aQuestion.labelStyle != "row"){
                aQuestion.bsColSize = Math.round(12/aQuestion.answer.aValues.length);
              }else{
                aQuestion.bsColSize = Math.round(12/aQuestion.answer.aValues.length);
              }
            }else{
              aQuestion.bsColSize = 1;
            }
            // push question object with appended answer object and some order fields
            q.push(aQuestion);
          }else if(typeof aQuestion.isPrompt != "undefined" && aQuestion.isPrompt == 'true'){
            q.push(aQuestion);
          }else{
            console.log("question does not have linking answer.")
          }
      }
    );
    console.log(q);
    if(q.length > 0){
        return q;
      }
  }
  return false;
}
Template.surveyTake.helpers({
  surveyTitle: function () {
    var survey =VH.Survey.findOne();
    if(survey && typeof survey.title != "undefined"){
      return survey.title;
    }
  },
  isPatient : function(){
    // force a patient rendering if the pathname is appropriate
    if(Meteor.userId() && VH.Survey.isPatient() ){
      return true;
    }
    var w= window.location;
    if(typeof w != "undefined" && w && typeof w.pathname != "undefined"){
        w = w.pathname.split('/');
        console.log(w);
        if(w.length > 1){
          // not sure here.. if we're not logged in then we're probably not a patient...
          if(w[1] == 'sTxt' || w[1] == 't'){
            // force a patient view when not logged in??
            return true;
          }
        }
      }
    return false;
  },
  buildQuestions : function(){
    var q = buildSurveyQuestions();
    if(q){
      console.log('returning' + q);
      return q;
    }else{
      console.log('attempting to return false');
      return false;
    }
  }
});
