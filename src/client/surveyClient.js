

  var getSurveyCSV = function(){
    // return a list dynamically based on an item that will filter the sub values to only include what is needed....
    return VH.SurveyResponses.find();
  };

  var getSurveyResponses = function () {
    var r = VH.SurveyResponses.find(),
        q = VH.Survey.findOne({_id : Session.get('selectedSurvey')});
    if(r && r.count() !== 0 && q){
      var response =  VH.SurveyResponses.find();
      count = undefined;
      return response.map(function(doc,index,cursor){
              totalScore = 0;
              console.log("response map");
              if(typeof doc.surveyResponse != "undefined"){
                doc.surveyResponse.filter(function(a,i){
                  if(typeof doc.surveyResponseQ == "undefined"){
                    doc.surveyResponseQ = [];
                  }
                  var questionL = _.findWhere(q.questions,{order:i+1});
                  doc.surveyResponseQ.push({
                      answer : a,
                      guid : (i+1) +''+ doc._id,
                      question : questionL
                  });
                  var numCheck = parseInt(a);
                  if(numCheck){
                      totalScore = totalScore + numCheck;
                  }else{
                    console.log('parse int failed');
                  }
                });
              }
        doc.title = q.title;
        doc.totalScore = totalScore;
        // figure out owner of current survey
        doc.providerId = q.owner;
        doc.organization = q.organization;
        doc.timestampF = new moment(doc.timestamp).format("hA - YYYY MM DD");
        // format datestamp?
        return doc;
      });
    }
    else
      return false;
};

Template.surveyCalculations.helpers({
  listCalculations: function () {
    // ...
    return VH.SurveyCalc.find().map(function (post) {
      // ...
      var title = VH.Survey.findOne({_id : post.surveyId},{title:1});
      var ownerName = VH.Survey.profileName(post.owner);
      if(typeof ownerName != "undefined" && ownerName && ownerName != ''){
        post.ownerName = ownerName;
      }else{
        post.ownerName = false;
      }

      if(typeof title != "undefined" && title != null && title && typeof title.title != "undefined" && title.title && title.title != ''){
        post.surveyTitle = title.title;
      }
      return post;
    });
  },
  selectedCalculation : function(){
    var selectedCalc = Session.get("selectedCalc");
    if(typeof selectedCalc != "undefined" && selectedCalc){
      return selectedCalc;
    }
    return false;
  },
  getSelectedCalc : function(){
    return VH.SurveyCalc.findOne({_id : Session.get("selectedCalc")});
  }
});
Template.surveyCalculations.events({
  'click .surveyCalc td': function () {
    Session.set("selectedCalc",this._id);
    // ...
  }
  // hmm sort of a hack.. implement better...
  
});
Template.surveyAnswers.helpers({
 getUserAnswers: function () {
    // list answers from sub
    return VH.SurveyAnswers.find();
  },
  getOneAnswer: function () {
    var selectedAnswer = Session.get("selectedAnswer");
    if(selectedAnswer){
      return VH.SurveyAnswers.findOne({_id : selectedAnswer});
    }
    return false;
  }
});

Template.surveyAnswers.events({
 "click .surveyAnswer" : function(){
      Session.set("selectedAnswer",this._id);
  }
});

/*
Template.survey.onCreated(function(){
  if(Meteor.userId()){
    var org = Session.get("organization");
    var selectedSurvey = Session.get("selectedSurvey");

    //if(typeof org != "undefined" && org){
    //  this.subscribe("surveyOrgUsers",org);
    //}
  
  }
});
*/

Template.surveyResponsesTableView.onCreated(function(){
  if(Meteor.userId()){
    var org = Session.get("organization");
    var selectedSurvey = Session.get("selectedSurvey");
    if(typeof org != "undefined" && org){
      this.subscribe("surveyResponses",selectedSurvey,org);
    }
  }
})
Template.survey.helpers({
  getSurveyTitle : function(){
    var q = VH.Survey.findOne(Session.get("selectedSurvey"));
    if(q && q.title != "undefined")
      return q.title;
    else 
      return false;
  },
  getUserSurveys: function () {
    // to do filter survey (clientside only?)
    if(VH.Survey.isPatient()){
      var u = Meteor.user();
      if(u && typeof u.services != "undefined" && typeof u.services.survey != "undefined" ){
        // to do date filters based on services.surveyTimes
        return VH.Survey.find({_id : {"$in" : u.services.survey} });
      }

    }else{
      // exclude some surveys if is provider redothis
      if(VH.Survey.isProvider()){
        // don't show 'templates'
        return VH.Survey.find({owner:Meteor.userId(),templateType : {"$ne" : "all"}},{sort:{templateType:-1}}).map(function (post) {
          // ...
          // get owner name

          var theOwner = VH.Survey.profileName(post.owner);
          if(typeof theOwner != "undefined" && theOwner){
            // maybe remove this call and replace with owner mongo id...
            post.ownerName = theOwner;
          }else{
            // was this a 'master' record?
            post.ownerName = false;
          }
          return post;
        });
      }else{
        console.log('not provider?');
        return VH.Survey.find().map(function (post) {
          // ...
          // get owner name

          var theOwner = VH.Survey.profileName(post.owner);
          if(typeof theOwner != "undefined" && theOwner){
            // maybe remove this call and replace with owner mongo id...
            post.ownerName = theOwner;
          }else{
            // was this a 'master' record?
            post.ownerName = false;
          }
          return post;
        });
      }
    }
  },
  getSelectedSurvey : function(){
    return VH.Survey.findOne({_id: Session.get("selectedSurvey")});
  },
  hasSelectedSurvey : function(){
    return !Session.equals("selectedSurvey",false);
  },
  isSelectedSurvey : function(){
    return Session.equals("selectedSurvey",this._id);
  },
  getSurveyQuestions: function(){
  // filter results...
    var q = VH.Survey.findOne(Session.get("selectedSurvey"));
    return ( q ? q : false);
   },
  getUserAnswers: function () {
    // list answers from sub
    return VH.SurveyAnswers.find();
  },
  getOneAnswer: function () {
    var selectedAnswer = Session.get("selectedAnswer");
    if(selectedAnswer){
      return VH.SurveyAnswers.findOne({_id : selectedAnswer});
    }
    return false;
  },
  getQuestionsCount : function(){
    console.log(this);
    if(typeof this.questions != "undefined"){
      var c = 0;
    this.questions.filter(function(q,i){
      if(typeof q.answerId != "undefined"){
        c++;
      }
    });
    // may return 1 or false or 0 ?
    if(c > 0)
      return c;
    else
      return 0;
    }
    return false;
  },isAdmin : function(){
    return VH.Survey.isAdmin();
  },isProvider : function(){
    return VH.Survey.isProvider();
  }
});
Template.ModalAddInvitation.onCreated = function(){
  this.subscribe("userSurveys",Session.get("organization"));
};
Template.ModalAddInvitation.helpers({
  getUserDoc: function () {
    return Meteor.users.findOne({_id : Meteor.userId()});
    // ...
  },
  getUsers : function(){
      var userQuery = {};
      var orgKey = 'roles.' + Session.get("organization");
      userQuery[orgKey] = 'patient';

      /* add verification check ... $elemMatch ?  {"$elemMatch" : { "emails.0.verified" : true}}
      
            Meteor.users.find({"emails.0.verified" : true},{"fields":{emails:1}}).fetch()
      */
      // if selected survey dont allow it to be added to additionals...
      var selectedSurvey = Session.get("selectedSurvey");
      if(typeof selectedSurvey != "undefined" && selectedSurvey){
        var selectedUser = Session.get("selectedUser");
        if(typeof selectedUser != "undefined" && selectedUser){
          userQuery._id = selectedUser;
          userQuery["services.survey"] =  {"$nin" : [selectedSurvey] };
          return Meteor.users.find(userQuery,{"fields":{profile:1,emails:1,'services.survey':1}}).map(function(o){
            return {_id : o._id, email : o.emails[0].address};
          });
        }
        // returns a list of users who aren't taking the survey
        userQuery["services.survey"] = {"$nin" : [selectedSurvey]};
        return Meteor.users.find(userQuery,{"fields":{profile:1,emails:1,'services.survey':1}}).map(function(o){
          return {_id : o._id, email : o.emails[0].address};
        });
      }else{

        return Meteor.users.find(userQuery,{"fields":{emails:1,profile:1,'services.survey':1}}).map(function(o){
          console.log(o);
          if(typeof o.profile != "undefined" && typeof o.profile.firstName != "undefined" && typeof o.profile.lastName != "undefined" && typeof o.emails != "undefined"){
            return {_id : o._id, name: o.profile.firstName + ' ' + o.profile.lastName,email : o.emails[0].address};
          }else if(typeof o.emails != "undefined" && typeof o.emails[0] != "undefined"){
            return {_id : o._id,name: o.emails[0].address,email : o.emails[0].address };
          }else{
            return false;
//            return {_id : o._id,name:o._id,email: 'none'};
          }
        });
      }
  },
  selectedUser: function(){
      console.log('\n\n\t selec u');
    var u = Session.get("selectedUser");
    console.log(u);
    if(typeof u != "undefined" && u){
      return u;
    }
    console.log('return false');
    return false;
  },
  selectedSurveyTitle:function(){
    var q = Session.get("selectedSurvey");
    if(typeof q != "undefined" && q) {
      var r = VH.Survey.findOne({_id : q});
      if(typeof r != "undefined" && r && typeof r.title != "undefined"){
        return r.title;
      }
    }
    return false;
  },
  hasSurvey:function(){
    console.log('\n\n\t selec');
    var s = Session.get("selectedSurvey");
    if(typeof s != "undefined" && s){
      return s;
    }
    return false;
  },
  getSurveys:function(){
    // eventually only return the surveys the client does NOT belong to.. 
    // also filter to not show templates and anything created by owner '1' 
    // which shouldn't be an issue for new installations....
    var s = VH.Survey.find({owner:{"$ne" : "1"},templateType : {"$exists" : 0}}).fetch();
    console.log(s);
    if(s && s.length > 0){
      var u = Session.get("selectedUser");
      if(u){
        var u = Meteor.users.findOne({_id : u},{"fields" : {"services.survey" : 1} });
        if(u && typeof u.services != "undefined" && typeof u.services.survey != "undefined" && u.services.survey != null){
          // filter selected users currently taken surveys...
          console.log(u.services.survey);
          var r=  [];
          console.log(s);
          s.filter(function(theO){
            if(typeof theO != "undefined"){
              console.log(theO);
              if(_.indexOf(u.services.survey,theO._id) == -1){
                r.push(theO);
              }
            }
          });
          if(r.length > 0){
            return r;
          }
        }else{
          return s;
        }
      }else{
        return s;
      }
    }
    return false;
  },
  getSurveyUsers:function(){
    var selectedSurvey = Session.get("selectedSurvey");
    if(typeof selectedSurvey != "undefined"){
      return Meteor.users.find({"services.survey" : {"$in" : [selectedSurvey] } },{"fields":{profile:1,emails:1}}).map(function(o){
        // map last hit from surveyResponses...
          var responses = VH.SurveyResponses.findOne({surveyId : selectedSurvey,owner:o._id},{"$orderby":{timestamp:-1}});
          var timestamp = false;
          if(responses && typeof responses.timestamp != "undefined"){
            timestamp = new moment(responses.timestamp).format("hA - YYYY MM DD") ;
          }
          return {_id : o._id, name: o.profile.firstName + ' ' + o.profile.lastName, email : o.emails[0].address, lastHit : timestamp};
        });
    }
    return false;
  },
  hasDates:function(evt,tmpl){
    // probably wont be reactive ... hmm
    var hasDates = Session.get("hasDates");
    if(typeof hasDates != "undefined"){
      return hasDates;
    }
    return false;
  }
});
Template.ModalAddInvitation.rendered = function () {
  // ... subscribe
  Session.set("hasDates",undefined);
  // hmmm..... is this happening???
  this.subscribe("userList",Session.get("organization"));
  this.subscribe("userSurveys",Session.get("organization"),Session.get("selectedUser"));

};
Template.ModalAddInvitation.events({
  'change .inviteDate':function(evt,tmpl){
    // on change and success show the end date...

    var start = tmpl.find(".inviteStartDate");
    var end = tmpl.find(".inviteEndDate");
    if(start && end && start.value != "undefined" && start.value != '' && end.value != "undefined" && end.value != ''){
      Session.set("hasDates",true);
      return true;
    }else{
      Session.set("hasDates",false);
      console.log('missing dates');
    }
    return false;
  },
  'change .inviteUser': function (evt,tmpl) {
    // ...
      var user = tmpl.find(".inviteUser");
      if(typeof user != "undefined" && user.value != "undefined"){
        Session.set("selectedUser",user.value);
        // next if in patients route subscribe to usersSurveys?
        Meteor.subscribe("usersAvailableSurveys",Session.get("selectedUser"),Session.get("organization"),function(e,r){
          console.log(e);
          console.log(r);
        });

        return true;
      }
      return false;
  },
  'change .invitationSurvey' : function(evt,tmpl){
      var survey = tmpl.find(".invitationSurvey");
      if(typeof survey != "undefined" && survey.value != "undefined" && survey.value != ''){
        Session.set("selectedSurvey",survey.value);
        return true;
      }
      return false;
  },
  'click .addUserToSurvey':function(evt,tmpl){
    var user = Session.get("selectedUser"), survey = Session.get("selectedSurvey");

    if(typeof user != 'undefined' && user && typeof survey != "undefined" && survey){


      var startDate = tmpl.find(".inviteStartDate");
      var endDate = tmpl.find(".inviteEndDate")
      if(startDate && endDate && startDate.value && endDate.value){
        console.log(user);
        console.log(survey);
        console.log(startDate.value);
        console.log(endDate.value);
        console.log(Session.get("organization"));
      Meteor.call("addSurveyToUser",user,survey,startDate.value,endDate.value,Session.get("organization"),function(e,r){
        if(typeof e == "undefined"){
          console.log(r);
          // probably reset form..
           $('.modal').modal('hide');
          VH.Alert.info("Added user to survey");
          Session.set("selectedUser",undefined);
          // this command resets for patient view... possibly check window.url?
          Session.set("selectedSurvey",undefined);
          Session.set("hasDates",undefined);

         return true;
        }else{
          console.log(e);

        }
        return false;
      });
      return true;
      }else{
        VH.Alert.warning("Missing start/end dates.");
      }
    }
    return false; 
  },
  'click .cancelUser':function(){
    //Session.set("selectedUser",undefined);
    Session.set("selectedSurvey",undefined);
    Session.set("hasDates",false);
    $('.modal').modal('hide');
  }
});
Template.survey.events({
  // user selects a survey to edit
  'click .survey': function () {
      if(!Session.equals("selectedSurvey",this._id)){
        // gets invites, meteor users .. probably want to examine that 
        // and also responses
        Session.set("selectedSurvey",this._id);
        //Meteor.subscribe("inviteFromManager",Session.get("selectedSurvey"));
        // must be admin...
        //if(typeof responseSub != "undefined"){
        //  responseSub.stop();
        //}
        //Meteor.subscribe("surveyResponses",this._id,Session.get("organization"),function(e,r){
        //    console.log(e);
        //    console.log(r);
        //});
      }
  },
  'click .clearSelectedAnswer':function(){
    Session.set("selectedAnswer",false);
  },
  // user decides to make a new survey when one is already selected for editing
  "click .clearSelectedSurvey" : function(){
      Session.set("selectedSurvey",false);
  },
  "click .surveyAnswer" : function(){
      Session.set("selectedAnswer",this._id);
  },
  "click .saveCsv" : function(evt,tmpl){
    var filename = tmpl.find('.csvName');
    if(typeof filename != "undefined" && filename.value != ''){
      filename = filename.value
    }else{
      filename = 'untitled.txt';
    }
    var checkboxInputs = ['f_id','f_owner','f_timestamp','f_surveyId','f_surveyResponse'];
    var checkboxFilter = [];
    checkboxInputs.filter(function(input){
      var iCheck = tmpl.find('.' + input);
      if(typeof iCheck != "undefined" && iCheck != null && iCheck){
         //console.log(iCheck.checked);
         if(iCheck.checked){
          checkboxFilter.push(input.split('_')[1]);
         }
      }
     
    });
    if(checkboxFilter.length === 0){
      // include all fields...
      checkboxInputs.filter(function(a){ 
        if(a =="f_id"){
          checkboxFilter.push('_id');
        }else{
          checkboxFilter.push(a.split('_')[1]); 
        }
      });
    
    }
    // can add user filter here...
    var blob = new Blob(VH.SurveyResponses.find().map(function(o){
    var r=[];
    // eventually store the users name as a field too for only specific users ? Maybe the provider .. not clear yet
  
      console.log(checkboxFilter);
    // read values from checkboxes ...
    for (var key in o) {
      console.log(key);
      if (o.hasOwnProperty(key) && _.indexOf(checkboxFilter, key) != -1 ) {
        if(key == "surveyResponse"){
          // generate total ans score totalScore field ?
          var total= 0;
          o[key].filter(function(a){
            if(a){
              var integer = parseInt(a);
              if(integer){
                total += integer;
              }
            }
          });
          r.push(total);
        }else{
        // if key is response... hmmm
          r.push( o[key]);
        }
      }
    }
      return r.join()+'\n';
    }), {type: "text/plain;charset=utf-8"});
    saveAs(blob, filename);
  }
});
Template.surveyResponses.helpers({

  getSurveyResponses: function () {
    return getSurveyResponses();
  },
  getSurveys : function(){
    var survey = Session.get("selectedSurvey");
    if(survey){
      return VH.Survey.find({_id : survey});
    }
    return VH.Survey.find();
  }
});
/* eventually to become 'reports' */
Template.surveyResponsesTableView.helpers({
  getSurveyCSV : function(){
    return getSurveyCSV();
  },
  getSurveyResponses: function(){
    return getSurveyResponses();
  },
  getSurveyResponseKeys : function(){
    // dynamically returns keys for the table generation...

    var r=[];
    // this isnt calculating totalll.. aghhh
    VH.SurveyResponses.find().map(function(o,i){
      if(i=== 0){
        for (var key in o) {
          if (o.hasOwnProperty(key)) {
            r.push(key);
          }
        }
      }
    }
    );
    if(r.length > 0){
      return r;
    }
    return false;
  }
});
Template.ModalCreateSurvey.rendered = function () {
  // ...

  Session.set("surveyTemplate",undefined);
};
Template.ModalCreateSurvey.events({
  'change .surveyTemplate': function (evt,tmpl) {
      var template = tmpl.find(".surveyTemplate");
      if(typeof template != "undefined" && typeof template.value != "undefined"){
        if(template.value == ''){
          console.log('no value');
          Session.set("surveyTemplate",undefined);

        }else{
          console.log(template.value);
            Session.set("surveyTemplate",template.value);

        }
      }
    // ...
  },
  'change .templateType' : function(evt,tmpl){
    console.log(tmpl.find(".templateType").value)
  }
});

Template.ModalCreateSurvey.helpers({
  getAvailableSurveyTemplates: function () {
    // map out name of the creator?
    // filter publications when ready.... redo subs
    return VH.Survey.find({organization:{"$in" : [Session.get("organization"),'admin'] },templateType : {"$in" : ['all','org'] } }).map(function (post) {
      // ...
      var name = VH.Survey.profileName(post.owner);
      if(typeof name != "undefined" && name){
        post.ownerName = name;
      }
      if(typeof post.templateType == "undefined" || post.templateType == ''){
        post.TemplateType = false;
      }
      return post;
    });
  },
  hasSelectedSurveyTemplate : function(){
    return !Session.equals("surveyTemplate",undefined)
  }
});
