Meteor.methods({
	sendCalculationEmail : function(to,subject,text,org,senderId){
		if(typeof senderId == "undefined" || senderId == null || !senderId){
			if(typeof this.userId != "undefined"){
				senderId = this.userId;
			}else{
				return false;
			}
		}
		// make sure 'to' address exists within meteor users
		Email.send({
			to : to,
			from : 'no-reply@varsahealth.com',
			subject : subject,
			text : text
		});
	},
	insertBaseMeasures : function(org,userId){
		// look up the base measures 
		// do i have access to this.userId() ?
		console.log('insert base measures from ' + userId + ' to ' + org);

		if(typeof userId == "undefined" || userId == null || !userId && typeof this.userId != "undefined" && this.userId != null){
			userId = this.userId;
		}else if(typeof userId == "undefined" && (typeof this.userId == "undefined" || this.userId == null || !this.userId)){
			return false;
		}
		// relinquish this precheck.... or figure out if we can find the creatorId ?
		//if(VH.Survey.isAdmin(org,userId) || VH.Survey.isProvider(org,userId)){
			// precheck
			var preCheck = VH.Survey.findOne({owner:userId});
			if(preCheck){
				console.log("user already has surveys");
				return false;
			}
			console.log(preCheck);
			var measures = VH.Survey.find({owner:'1'}).fetch();
			if(typeof measures != "undefined" && measures.length > 0){
				// check if userId is admin
				var record_ids = []
				measures.filter(function(o){
						var newObj = _.omit(o,['_id','owner','organization']);
						newObj.owner = userId;
						newObj.organization = org;
						// hard coded....
						var record_id = VH.Survey.insert(newObj);

						if(record_id){
							record_ids.push(record_id);
							
							if(o.title == 'Personal Health Questionnaire'){
								// get measure
								console.log("linking PHQ8\t" + record_id + ' to userId ' + userId)
								Meteor.call("linkPHQ8",record_id,userId,org);
							}
							if(o.title == 'Zung Self-rating Anxiety Scale'){
								console.log("linking Zung\t" + record_id + ' to userId ' + userId)
								Meteor.call("linkZung",record_id,userId,org);
							}
						}
						
				});
				// insert survey measure?
			    return record_ids;
			}else{
				console.log('could not find base measures to insert');
			}
		//}else{
		//	console.log('user does not have permissions to do this');
		//}
	},
	linkPHQ8 : function(record_id,userId,org){
		// make sure userId matches to org?
		if(typeof record_id != "undefined" && record_id && typeof userId != "undefined" && userId && typeof org != "undefined" && org){
			if(VH.Survey.isAdmin(org,userId) || VH.Survey.isProvider(org,userId)){
				// make sure record_id is a survey in the org?
				// only let providers make calcs for the surveys they own
				var surveyQuery = {_id : record_id, organization:org};
				if(VH.Survey.isProvider(org,userId)){
					surveyQuery.owner = userId;
				}

				var surveyCheck = VH.Survey.findOne(surveyQuery);

				if(typeof surveyCheck == "undefined" || !surveyCheck || surveyCheck == null){
					console.log('survey check failed not linking phq8');
					return false;
				}
				var template = {
						"action" : [
							{
								"type" : "notification",
								"receiver" : "provider",
								"calcType" : "sum",
								"condition" : "gte",
								"condition_value" : 0
							},
							{
								"type" : "email_notification",
								"receiver" : "provider",
								"calcType" : "sum",
								"condition" : "gte",
								"condition_value" : 15
							}
						],
						"surveyQuestions" : "all",
						"interpretation" : [
							{
								"title" : "No significant depressive symptoms",
								"iMin" : "0",
								"iMax" : "4"
							},
							{
								"title" : "Mild depressive symptoms",
								"iMin" : "5",
								"iMax" : "9"
							},
							{
								"title" : "Moderate depressive symptoms",
								"iMin" : "10",
								"iMax" : "15"
							},
							{
								"title" : "Moderately depressive severe symptoms",
								"iMin" : "15",
								"iMax" : "19"
							},
							{
								"title" : "Severe depressive symptoms",
								"iMin" : "20",
								"iMax" : "24"
							}
						]
				};
				template.owner = userId;
				template.organization = org;
				template.surveyId = record_id;
				return VH.SurveyCalc.insert(template);
			}else{
				console.log("linkPHQ8 tried to link to non admin/provider account");
			}

		}else{
			console.log("PHQ8 linker failed with missing parameters");
		}

	},
	linkZung : function(record_id,userId,org){
		if(typeof record_id != "undefined" && record_id && typeof userId != "undefined" && userId && typeof org != "undefined" && org){
			if(VH.Survey.isAdmin(org,userId) || VH.Survey.isProvider(org,userId)){
				// make sure record_id is a survey in the org?
				// only let providers make calcs for the surveys they own
				var surveyQuery = {_id : record_id, organization:org};
				if(VH.Survey.isProvider(org,userId)){
					surveyQuery.owner = userId;
				}

				var surveyCheck = VH.Survey.findOne(surveyQuery);

				if(typeof surveyCheck == "undefined" || !surveyCheck || surveyCheck == null){
					console.log('survey check failed not linking phqZung');
					return false;
				}
				var template = 
					{	"action" : [
						{
							"type" : "notification",
							"receiver" : "provider",
							"calcType" : "sum",
							"condition" : "gte",
							"condition_value" : 0,
							"q" : "all"
						},
						{
							"type" : "email_notification",
							"receiver" : "provider",
							"calcType" : "sum",
							"condition" : "gte",
							"condition_value" : 36,
							"q" : "all"
						}
						],
						"interpretation" : [
						{
							"title" : "Need for further medical assessment of GAD",
							"iMin" : "36",
							"iMax" : "80"
						}
						]
					};
				template.owner = userId;
				template.organization = org;
				template.surveyId = record_id;
				return VH.SurveyCalc.insert(template);
			}
		}else{
			console.log("missing params");
		}


	},
	surveyCalculations : function(surveyId,surveyTakerId,lastResponseId){
		now = new moment();
		lastResponse = VH.SurveyResponses.findOne({ _id : lastResponseId }),
		surveyCreator = VH.Survey.findOne({_id : surveyId},{"fields" : {owner:1,organization:1,title:1} });
		if(typeof surveyCreator == "undefined" || surveyCreator == null || !surveyCreator){
			console.log("could not find survey");
			return false

		}else if(typeof surveyCreator.owner != "undefined"){
			console.log(surveyCreator);
			// try make provider global so filter has access
			provider = surveyCreator.owner;
			patient = surveyTakerId;
			//var admin = '';

			if(Roles.userIsInRole(surveyCreator.owner, 'provider', surveyCreator.organization)){
				console.log('survey creator is the provider');
				admin = surveyCreator.owner;
			}else if( Roles.userIsInRole(surveyCreator.owner, 'admin', surveyCreator.organization )){
				// what do do in this case ... the admin is providing the survey... tricky set patient/admin to same?
				console.log('survey creator is an administrator');
				admin = surveyCreator.owner;
			}
			//if(admin == ''){
				// how to figure out survey administrator?
				// what if theres multiples?
				//var rolesKey= roles + '.' + surveyCreator.organization
				// theres no admin grouP!?
				//var adminCheck = Meteor.users.find({organization : surveyCreator.organization, rolesKey : })

			//}
			
			// determine if the role of this person
		}
		console.log(provider);
		userCheck = Meteor.users.findOne({_id:surveyTakerId, "services.survey" : {"$in" : [surveyId] } },{"fields": { "services.survey" : 1, creatorId:1,profile:1}} );
		// one calc per survey.... dont make me regret this..
		var calculations = VH.SurveyCalc.findOne({surveyId : surveyId});
		console.log(calculations);
		if(typeof calculations.action != "undefined" && calculations.action.length > 0){
			// parse out response items to be integers...
			calculations.action.filter(function(obj,x){
				var calcValue = 0;
				var questions = [];
				var score = 0;
				var perform = false;
				var noteData = '';
				// double check it matches list of 
				// accepted conditions... 
				// do calc type 
				if(typeof obj.calcType != "undefined"){
					switch(obj.calcType)	{
						case "sum":
							
							if(typeof obj.q != "undefined" && obj.q != null && obj.q && obj.q != ''){
								// take care for prompt types to not calculate those values...
							}else{
								lastResponse.surveyResponse.filter(function(o){
									var theInt = parseInt(o);
									if(theInt){
										questions.push(theInt);
									}
								});
								// next apply application (sum items)
								console.log('reducing');
								console.log(lastResponse.surveyResponse);
								console.log(questions);
								score = questions.reduce(function(a,b){return a+b;},0);
								console.log(score);
								//parse all of them
							}
						// determine which questions to use in scoring
							break;
						case "aggsum":
							break;
					}
					
					if(typeof obj.condition_value != "undefined" && typeof calcValue != "undefined" && questions.length > 0){
						console.log("anaylzing condition");
						console.log(obj.condition);
						// map conditions to mongo selectors?
						switch(obj.condition)	{
							case "gt":
								if( score > obj.condition_value){
									perform = true;
								}
								break;
							case "lt":
								if( score < obj.condition_value){
									perform = true;
								}
								break;
							case "et":
								if( score === obj.condition_value){
									perform = true;
								}
								break;
							case "gte":
								console.log('gte');
								console.log(obj.condition_value);
								console.log(score);
								if( score >= obj.condition_value){
									perform = true;
									console.log('set perform to true');
								}
								break;
							case "lte":
								if( score <= obj.condition_value){
									perform = true;
								}
								break;
							case "ne":
								if(score != obj.condition_value){
									peform = true;
								}
								break;
						}

						if(perform){
							console.log('performing action');
							// match interpretation and then send out template
							if(typeof calculations.interpretation != "undefined" && calculations.interpretation.length > 0){
								console.log('has interpretations');
								calculations.interpretation.filter(function(i,z){

									console.log(i);
									//console.log(score);
									console.log('score : ' + typeof score + score);
									var min = parseInt(i.iMin);
									console.log('min : ' + typeof min + ' ' + min);
									var max = parseInt(i.iMax);
									console.log('max : ' + typeof max + ' ' + max);

									var receiver = '';
									// default... probably create another template designed for patient and one
									// for provider and one for admin and one for superadmin?
									var notificationType = 'PatientCompleteSurvey';
									// dont do score checks here... only used to map out
									// kinda tricky...
									// this will be ok if interpretations are in appropriate order
									// to do handle reverse scale questions...
									if(score <= max || z == calculations.interpretation.length -1){
										noteData = 'score : ' + score + i.title;
										 // bare minimum of fields
										var nData = {userName : userCheck.profile.firstName + ' ' + userCheck.profile.lastName,
										              surveyId : surveyId,
										              surveyTitle : surveyCreator.title};
										// do this notification ...
										// type...
										// determine receiver
										// get user mongo id of various group
										console.log(obj);
										console.log('receiver' + receiver);
										var subject ='Email from varsa health', text ='Email from varsahealth';
										switch(obj.receiver){
											case "provider":
												// userCheck.creatorId - this could be created by admin/provider ?
												// if you can get creator of survey type?
												// provider is usually blank...
												receiver = admin;
												// add some extra fields for the provider to read
												nData.score = score;
												nData.interpretation = i.title;
												nData.responseId = lastResponseId;
												// for email
												subject = 'Patient Survey - Alert';
												//
												text = 'One of your patients recently completed the ' + surveyCreator.title + ' around ' + now.format("dddd, MMMM Do YYYY, h:mm:ss a") +' and needs attention. \nLogin at https://'+surveyCreator.organization+'.varsahealth.com/ .';
												break;
											case "admin":
												receiver = admin;
												break;
											case "patient":
												receiver = surveyTakerId;
												text = 'Thank you for completing ' + surveyCreator.title;
												// change template for recipient
												break;
											case "superadmin":
												receiver = provider;
												break;
											case "circleofcare":
												console.log('coc not supported yet.');
												return false;
												break;

										}
										console.log(receiver);

										
										switch(obj.type)	{
											case "notification":
												 VH.Notification.create({
										            recipientId: receiver,
										            type: notificationType,
										            data: nData});
												break;
											case "email":
												if(subject != '' && text != ''){
													var findEmail = Meteor.users.findOne({_id : receiver},{emails : 1});

													if(findEmail && typeof findEmail.emails != "undefined" && typeof findEmail.emails[0] != "undefined" && typeof findEmail.emails[0].address != "undefined"){
														Meteor.call("sendCalculationEmail", findEmail.emails[0].address,subject,text,surveyCreator.organization,surveyCreator.owner)
													}else{
														console.log("could not find email for receiver : " + receiver);
													}
												}else{
													console.log('no text or email to send... within calculation receiver : ' + receiver );
												}
												break;
											case "email_notification":
												console.log('doing email + notification for ' + receiver + '\n with data \n' + nData.interpretation);
												console.log(subject);
												var findEmail = Meteor.users.findOne({_id : receiver},{emails : 1});

												if(subject != '' && text != '' && receiver != ''){
													// convert receiver id..
													// do verification check?
													if(findEmail && typeof findEmail.emails != "undefined" && typeof findEmail.emails[0] != "undefined" && typeof findEmail.emails[0].address != "undefined"){
														Meteor.call("sendCalculationEmail",  findEmail.emails[0].address ,subject,text,surveyCreator.organization,surveyCreator.owner);
													}else{
														console.log('could not find email address to send notification for receiver userid ' + receiver);
													}
												}else{
													console.log('email notification missing params :\t receiver:\t' + receiver + ' subject:\t' + subject + ' text:\t' + text );
												}
												// do both
												console.log(notificationType);
												VH.Notification.create({
										            recipientId: receiver,
										            type: notificationType,
										            data: nData
										        });

												break;
										}
									}
								});
							}
						}

					}else{
						// no action to perform
						console.log('no action to perform ... condition not met' + score + '\t' + obj.condition +'\t' + obj.condition_value);
					}
				}
			})
		}
		//console.log(lastResponse);
		// calculate sum based on calculations...
		//console.log(calculations);

	},
	notifyPatientCompleteSurvey : function(surveyTaker,survey,responseId){
		if(typeof surveyTaker != "undefined" && typeof survey != "undefined"){
			if( typeof surveyTaker != null && typeof survey != null){
				if( surveyTaker && survey){
					var surveyLookup = VH.Survey.findOne({_id : survey },{"fields" : { title : 1, owner: 1}});
					if(surveyLookup && typeof surveyLookup.title != "undefined"){
						// ensure user has  survey
						var userCheck = Meteor.users.findOne({_id:surveyTaker, "services.survey" : {"$in" : [survey] } },{"fields": { "services.survey" : 1, creatorId:1,profile:1}} );

						if(typeof userCheck != "undefined" && userCheck){
							Meteor.call("surveyCalculations",survey,surveyTaker,responseId);
							/*
							var data = {
					                  userId: surveyTaker,
					                  // consider ONLY storing survey id? doing title lookup in template? maybe...
					                  surveyId : survey,
					                  surveyTitle : surveyLookup.title,
					                  userName : userCheck.profile.firstName + ' ' + userCheck.profile.lastName,
					                  responseId : responseId
					                }
					          
							VH.Notification.create({
					                recipientId: surveyLookup.owner,
					                type: "PatientCompleteSurvey",
					                data: data
					            });
							// send notification to the person who made the patients account>??? userCheck.creatorId ?
					            // send notification to survey participant
					         VH.Notification.create({
					            recipientId: surveyTaker,
					            type: "PatientCompleteSurvey",
					            data: {
					              userName : userCheck.profile.firstName + ' ' + userCheck.profile.lastName,
					              surveyId : survey,
					              surveyTitle : surveyLookup.title,
					              responseId : responseId
					            }
					        });
							*/
					        return true;
					     }else{
					     	console.log('user ' + surveyTaker + ' failed user check with survey ' + survey );
					     	console.log(surveyLookup);
					     }
			     	}
			     	console.log('could not validate survey with taker');
					return false;
				}
			}
		}
		console.log("invalid or missing params");
		return false;
	},
	sendTextMsg : function(phone,msg){
		if(typeof Twilio == "undefined"){
			console.log("Twilio not found");
			return false;
		}
		if(Meteor.settings && typeof Meteor.settings.twilio != "undefined" && typeof Meteor.settings.twilio.sid != "undefined" && typeof Meteor.settings.twilio.token != "undefined" && typeof Meteor.settings.twilio.number != "undefined"){
			var tSid = Meteor.settings.twilio.sid,
			tToken = Meteor.settings.twilio.token,
				// add +1
			tNumber = Meteor.settings.twilio.number;
		}else{
			console.log("Twilio is not configured.")
			return false;
		}

		var twilio = Twilio(tSid, tToken);
		if(twilio){
			twilio.sendSms({
			    to:'+1'+phone, 
			    from: tNumber, 
			    body: msg 
			  }, function(err, responseData) { 
			    if (!err) { 
			      console.log(responseData.from); 
			      console.log(responseData.body); 
			    }
			});
		}else{
			console.log('is twilio configured?');
			return false;
		}
	},
	/*
	* A meteor.users collection approach to invitations - storing active survey participating in new meteor.users.services field
	*/
	addSurveyToUser : function(uId,surveyId,startTime,endTime,org){
		if(typeof uId != "undefined" && typeof surveyId != "undefined" && typeof org != "undefined" && typeof startTime != "undefined" && typeof endTime != "undefined"){
			// make sure uId belongs to org
			// check if survey does not already belong ...
			var query = {
				_id : uId 
			
			};
			
			// this query having issues in production
			
			//query['roles.'  + org] =  {"$exists" : true};

			var user = Meteor.users.findOne(query);

			if(user){

				var notificationType = "AdminInvitedToSurvey";
					
				var surveyLookup = VH.Survey.findOne({_id : surveyId},{"fields" : { title : 1}});
				if(typeof surveyLookup == "undefined" || !surveyLookup || typeof surveyLookup.title == "undefined" || typeof surveyLookup.title == "null" || !surveyLookup.title){
					console.log('\n\t could not find survey within surveyMethods.js line :251');
					return false;
				}

				if(typeof user.services == "undefined" ){
					if(Meteor.users.update({_id:uId},{"$set": { "services" : { "survey":[surveyId]} }})){
						if(Meteor.users.update({ _id: uId },{"$set" : { "services.surveyTimes" : {surveyId:surveyId,startTime:startTime,endTime:endTime}}} )){
							// send notification
							VH.Notification.create({
								recipientId: this.userId,
								type: notificationType,
								data: {
								  userId: uId,
								  // consider ONLY storing survey id? doing title lookup in template? maybe...
								  surveyId : surveyId,
								  surveyTitle : surveyLookup.title
								}
							});
							return true;
						}else{
							// probably removing users.services ...
							console.log("could not initally set services.surveyTimes");
						}
					}else{
						console.log("could not initally set user.services");
					}
					// do  check on user.roles[org][0]
					// need clarification on notification type ::: how does this interact with vh-app-base-ui ? How do I add new notification types?
					// templates are named via the role of the user who is going to be taking it... so if the user who is invited is an admin
					// it will load 'AdminInvitedToSurvey' inside of vh-notification-templates/survey.html
					// who gets user invite .. control that here... forcing it to be 'Admin'
					//notificationType = VH.HELPERS.capitalizeFirstLetter(user.roles[org][0]) + "InvitedToSurvey";
					
					//return Meteor.users.update({_id:uId},{"$addToSet" : { "services.survey" : surveyId} });

					return false;
				}else{
				// if services not created... create it?
				// update user
				// think about adding stuff?
				// check to see if survey is valid & active?
					var newKey = 'services.surveyTimes.' + surveyId;
					var set = {"$set" : {} };
					set['$set'][newKey] = {
						startTime:startTime,
						endTime:endTime
					}
					// set time
					if(Meteor.users.update({ _id: uId }, set)){
						if(Meteor.users.update({_id:uId},{"$push" : { "services.survey" : surveyId} })){
							VH.Notification.create({
								recipientId: this.userId,
								type: notificationType,
								data: {
								  userId: uId,
								  // consider ONLY storing survey id? doing title lookup in template? maybe...
								  surveyId : surveyId,
								  surveyTitle : surveyLookup.title
								}
							});
							return true;
						}else{
							console.log("Could not push to services.survey");
						}

					}else{
						console.log('could not update surveyTimes on survey insert');
					}
					return false;
				}
			}else{
				console.log(user);
				console.log(query);
				console.log(uId);
			}
			return false;
		}
		console.log('add survey to user missing params');
		return false;
	}
});