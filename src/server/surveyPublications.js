Meteor.publish("surveyCalc",function(org,id){
	if(typeof this.userId == "undefined" || !this.userId){
		this.ready();
		return false;
	}
	if(VH.Survey.isProvider(org,this.userId) || VH.Survey.isAdmin(org,this.userId)){
		if(typeof org == 'undefined' && typeof this.userId){
			// look up org via userid ?
			console.log("\tno org provided for surveyCalc pub");
			this.ready();
			return false;
		}else if(typeof org != 'undefined' && typeof id == 'undefined'){
			// return a list of calculations....
			console.log('returning list in surveyCalc pub');
			return VH.SurveyCalc.find({organization : org});
		}else if(typeof org == "undefined" && typeof id == "undefined"){
			this.ready();
			return false;
		}else if(typeof org != "undefined" && typeof id != "undefined" && id){
			return VH.SurveyCalc.find({organization : org,_id : id})
		}
		return VH.SurveyCalc.find({organization : org});
	}else{
		console.log('Cannot access survey calc pub');
		this.ready();
		return false;
	}
});

Meteor.publish("misfit",function(code){
	if(typeof this.userId != "undefined" && this.userId != null && this.userId){
		console.log('attempting publication to userId' + this.userId + ' with misfitCode ' + code);
		if(typeof code != "undefined"){
			// check if its in vh_survey_activity...
			// find a value that doesn't have an expired token?
			var activityCheck = Misfit.findOne({_id:code,owner:this.userId});
			
			if(typeof activityCheck == "undefined" || activityCheck == null){
				console.log('calling exchange');
				Meteor.call("misfitExchange",code,this.userId);
			}else{
				console.log("Record exists for this code... attempting to recall an existing code in publication");
				console.log(activityCheck);
			}
			// make sure it doesnt have an error!!
		}
		// get the code...

		return Misfit.find({token : {"$exists" : true},owner:this.userId},{"fields": {token:1} });

	}else{
		console.log("User not logged in");
	}

});
Meteor.publish("misfitSleepData",function(uId){
	if(typeof uId != "undefined"){
		console.log("gettin sleep data" + uId);
		return Misfit.find({owner:uId});
	}else{
		console.log('uId not available');
	}

});
// returns list of surveys user is not taking...
Meteor.publish('usersAvailableSurveys',function(org,userId){
	if(typeof userId == 'undefined'){
		var userId = this.userId;
	}
	if(userId){
		// do backend map..
		// limit to provider/admin/superAdmin
		var surveys = Meteor.users.findOne({_id:userId},{"fields": { "services.survey" : 1, creatorId:1}} );
		if(surveys && typeof surveys.services != "undefined" && typeof surveys.services.survey != "undefined"){
			var surveys = surveys.services.survey;
			var s = [];
			console.log(surveys);
			if(typeof surveys == "array" || typeof surveys == "object"){
				surveys.filter(function(x){
					s.push(x);
				});
			}else{
				console.log('services.survey is not an array');
			}
			console.log(s);
			if(s.length > 0){
				//if userid is patient then we're referencing this to show which surveys to take otherwise
				// we're building a list of surveys the user not taking so they may be added
				if(VH.Survey.isPatient(org)){
					console.log('is patient');
					return [VH.Survey.find({organization:org,owner: surveys.creatorId, _id : {"$in" : s } }),VH.SurveyAnswers.find()];
				}

			// maybe return a digest of responses?
				return [VH.Survey.find({owner: surveys.creatorId, _id : {"$nin" : s } }),VH.SurveyAnswers.find()];
			}
		}else{
			console.log('else');
			// user is not assigned to survey
			// only show surveys that the user belongs to
			return [VH.Survey.find({organization:org ,owner:surveys.creatorId}),VH.SurveyAnswers.find()];
		}
	}
	this.ready();
	return false;
	});

Meteor.publish('getSurvey',function(uId,surveyId){
	// check if surveyId exist within meteor.users
	var uCheck = Meteor.users.findOne({_id : uId, "services.survey" : {"$in" : [surveyId] } },{"fields" : {_id : 1 ,"services.survey" : 1,"services.surveyTimes" :1 }});
	var now = moment().utc();
	var sCheck = VH.Survey.findOne({_id : surveyId},{fields: {issueDate : 1,issueInterval:1} }  );
	if(typeof sCheck == "undefined" || !sCheck){
		this.ready();
		return false;
	}
	var sIssue = moment(sCheck.issueDate).utc();
	// get day of week of sIssue

//	var sInterval = moment(sCheck.issueInterval).utc();
	//var btween = moment().isBetween(uCheck.services.surveyTimes[surveyId].startTime, uCheck.services.surveyTimes[surveyId].endTime, 'day');
	var sStart = moment();
	var sEnd = moment();
	switch(sCheck.issueInterval) {
	    case 'weekly':
	 	   if(now.day() == sIssue.day() || now.day() > 3){

		   }
	        break;
	    case 'biweekly':
	        break;
	    case 'daily':
	    	break;
	    case 'monthly':
	    	break;
	}
//	console.log(sCheck);
	console.log(sIssue.day());
	// modify duration day based on 0
	// now.day() sunday == 0 saturday == 6?
	if(now.day() == sIssue.day() || now.day() < durationDay){

	}
	console.log(now.day());
//	console.log(sInterval);
	/*
		     {label:"Weekly",value:"weekly"},
				        {label:"Bi-Weekly",value:"biweekly"},
				        {label:"Daily",value:"daily"},
				        {label:"Monthly",value:"monthly"}];
	*/
	//var sStartTime = 
	// check if start time is today or greater
	// get day of week of the startTime
	// get day of week of now
	// determine duration based on sStartTime
	// determine if now is < duration
	if(typeof uCheck != "undefined" && uCheck){
		// handle timeout stuff
		// check if a response has been made within the last day ? or within parameters of survey ...
		return[VH.Survey.find({_id : surveyId}),VH.SurveyAnswers.find()];
	}else{
		console.log('\nuser ' + uId + ' does not belong to ' + surveyId + '\n');
		this.ready();
		return false;

	}

});
Meteor.publish("orgProviderSurveys",function(org){
	if(typeof this.userId == "undefined" || !this.userId){
		this.ready();
		return false;
	}else{
		// check user is in org?
		if(VH.User.is(this.userId,'provider',org) || VH.User.is(this.userId,'admin',org)){
			// list all surveys in an organization .. .hmmm
			// or figure out the superadmin user and return those too...
			var superuserId = Meteor.users.findOne({"roles.__global_roles__" : {"$eq": ["superadmin"] } });
			console.log(superuserId);
			return VH.Survey.find({owner:{"$in":[this.userId,]},organization:org},{fields:{questions:0}});
		}
	}
});
Meteor.publish("orgSurveyTemplates",function(org){
	// only admins need this to let providers have access to assign
	if(typeof this.userId == "undefined" || !this.userId || typeof org == "undefined" || org == null || !org || !VH.Survey.isAdmin(org,this.userId) ){
		this.ready();
		return false;
	}
	return VH.Survey.find({owner:1,organization:org});

})

Meteor.publish("surveyUserTake",function(surveyId,userId,org){
	var survey = VH.Survey.find({organization:org,_id : surveyId});
	if(typeof survey != "undefined" && survey){
		var user = Meteor.users.findOne({_id : userId,"services.survey" : {"$in" : [surveyId] }   },{});
		// find the matching survey element in suvey times
		if(typeof user != "undefined"){
			if(typeof user.services != "undefined" && typeof user.services.surveyTimes != "undefined"){
				var surveyDates = _.findWhere({surveyId : surveyId},user.services.surveyTimes)
				// what data is needed from user? if any...
				return [survey,VH.SurveyAnswers.find()]		
			}
		}
	}
	this.ready();
	return false;
});

// return services.survey and services.surveyTimes
Meteor.publish("patientSurveyData",function(org){
	if(typeof this.userId != "undefined" && typeof org != "undefined"){
		// do additional patient check?
		if(VH.Survey.isPatient(org,this.userId)){
			var surveys = Meteor.users.findOne({_id:this.userId},{"fields": { "services.survey" : 1, creatorId:1}} );
			if(surveys && typeof surveys.services != "undefined" && typeof surveys.services.survey != "undefined"){
				// filter out surveys that are unavailable ?
				return VH.Survey.find({owner : surveys.creatorId, _id : {"$in" : surveys.services.survey}});
			}
		}else{
			console.log("uid : " + this.userId + ' is not a patient at ' + org);
		}
	}
	this.ready();
	return false;
})

Meteor.publish("surveyOrgUsers",function(org){
	if(typeof org == "undefined" || typeof org == null || !org || org == ""){
		this.ready();
		return false;
	}
	if(this.userId == 'undefined' || !this.userId || this.userId == '' || typeof this.userId == null){
		this.ready();
		return false;
	}
	var isAdmin = VH.Survey.isProvider(org,this.userId);
	if(!isAdmin){
		// check if isAdmin
		var isAdmin = VH.Survey.isAdmin(org,this.userId);
	}
	if(!isAdmin){
		this.ready();
		return false;
	}
	// produce list of users capable of taking surveys.... 
	var userQuery = {};
	var orgKey = 'roles.' + org;
	userQuery[orgKey] = 'provider';

	return [Meteor.users.find(userQuery,{fields:{emails:1,profile:1,"services.survey":1}}),VH.SurveyAnswers.find()];

});

Meteor.publish("surveyResponses",function(id,org){
	// pass an organization?
	/*
		THIS NEEDS WORK!! FIXX
	*/
	if(typeof org != "undefined"){
		if(typeof this.userId != "undefined"){
			var isAdmin = VH.Survey.isProvider(org,this.userId);
			//if(!isAdmin){
				// admins dont have access ? what about superadmins...
				// check if isAdmin
			//	isAdmin = VH.Survey.isAdmin(org,this.userId);
			//}
		}else{
			console.log("user not logged in");
			return false;
		}

	}else{
		console.log('org not passed!');
		var isAdmin = VH.Survey.isAdmin();
	}
	if(isAdmin){
		//var userQuery = {"services.survey" : {"$exists" : true} };
		//var orgKey = 'roles.' + org;
		//userQuery[orgKey] = {"$exists":true};
		//console.log(userQuery);

		if(typeof id == "undefined" || id == null || id == false){
		// add organization filters when branching out
			//console.log("returning all surveys, and all answers...");
			this.ready();
			return false;
		}else{
			//console.log("returning survey responses for survey ... ");
			// problematic if session var is set?
			// do another check for meteor users find to only return the users that belong to the survey??
			return VH.SurveyResponses.find({surveyId : id }) ;

		}
	}else{
		console.log('not permitted to view surveyResponses');
		this.ready();
		return false;
	}
});
Meteor.publish("surveyPreview",function(id,org){
	if(typeof org != "undefined" && (VH.Survey.isAdmin(org,this.userId) || VH.Survey.isProvider(org,this.userId) )){
		return[VH.Survey.find({_id : id}),VH.SurveyAnswers.find()];
	}
	console.log('did not return data' + id + org);
	this.ready();
	return false;

});
Meteor.publish("userSurveys",function(org,id){
	if(this.userId && typeof id == "undefined"){
		console.log(this.userId);
		// figure out user organization...
		// do a users filter for only patients
		// is this called in patients?
		// modify survey find query
		// hmm only show users in org that they invited??
		var userFind = {};
		userFind.creatorId = this.userId;
		var userGroupKey = 'roles.' + org;
		userFind[userGroupKey] = "patient";
		// todo filter by owner... add provider key to patient onboarding process...
		// {fields:{"services.survey":true}}
		if(VH.Survey.isSuperAdmin(this.userId)){
			console.log('is superadmin');
			// dont need to send back meteor users
			/*
			var returnSurveys = VH.Survey.find({}).map(function (post) {
				// ...
				// get owner name
				var theOwner = Meteor.users.findOne({_id : post.owner},{fields: {profile:1} } );
				if(typeof theOwner != "undefined" && typeof theOwner.profile != "undefined" && typeof theOwner.profile.firstName != "undefined" && typeof theOwner.profile.lastName != "undefined"){
					// maybe remove this call and replace with owner mongo id...
					post.ownerName = theOwner.profile.lastName + ', ' + theOwner.profile.firstName;
				}
			});
		*/
			var s = VH.Survey.find();
			var returnSurveys = s.map(function(p){
				return p.owner;
			});
			return [s,Meteor.users.find({owner : { "$in" : returnSurveys }},{fields:{profile:1} }),VH.SurveyAnswers.find({})];
		}
		else if(VH.Survey.isAdmin(org,this.userId) || VH.Survey.isProvider(org,this.userId)){
			// admins see all of their orgs?
			var superuserId = Meteor.users.findOne({"roles.__global_roles__" : {"$eq": ["superadmin"] } },{fields:{_id:1}});

			return [VH.Survey.find({organization:{"$in" : [org,'admin']},owner:{"$in" : [this.userId,superuserId._id,"1"] }}),VH.SurveyAnswers.find({}),Meteor.users.find(userFind,{fields:{"services.survey":true}} ) ];
		}
	}else if(typeof id != "undefined"){
		// limit this later to only get 'open' listings
		//userFind[userGroupKey] = {"$in" : ['provider','patient'] }
		return [VH.Survey.find({organization:org,_id : id}),VH.SurveyAnswers.find({}),Meteor.users.find( userFind , {fields:{"services.survey":true}} ) ];
	}
	else{
		this.ready();
		return false;
	}
});
// filters only the surveys that the user does not belong to in org?

Meteor.publish("userList",function(org){
	if(typeof org != "undefined" && (VH.Survey.isAdmin(org,this.userId) || VH.Survey.isProvider(org,this.userId) )){
		// probably find roles.org
		var findQuery = {};
		var tFields = {
			profile : 1,emails:1,"services.survey" : 1
		}
		var temp = 'roles.' + org;

		tFields[temp] = 1
		console.log(temp);
//		return [Meteor.users.find(findQuery,{fields: fields}),VH.Survey.find({}) ];
		return Meteor.users.find(findQuery,{fields: tFields})
	}else{
		this.ready();
		return false;
	}
	
});
Meteor.publish("userAnswers",function(org,id){
	if(typeof id != "undefined"){
		return VH.SurveyAnswers.find({_id : id });
	}else if(this.userId){
		// if is superadmin... return all / admin
		// get organization...
		if(VH.Survey.isAdmin(org,this.userId)){
			return VH.SurveyAnswers.find({});
		}else if(VH.Survey.isProvider(org,this.userId)){
			// return owner : 1 ?
			// users custom answers
			return VH.SurveyAnswers.find({owner:this.userId});

		}
	}else{
		this.ready();
		return false;
	}
});

Meteor.publish("userSurveyResponses",function(userId){
	// unless isAdmin return this.userId  left on for dev purposes
	if(typeof userId == "undefined" && this.userId){
		userId = this.userId;
	}else{
		this.ready();
		return false;
	}
	return VH.SurveyResponses.find({owner : userId});
});