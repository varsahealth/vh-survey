


Meteor.startup(function(){
//	if(check(BrowserPolicy,Object)){
  //  BrowserPolicy.content.allowDataUrlForAll("*.fonts.gstatic.com");
//for the tracking pixel
//BrowserPolicy.content.allowDataUrlForAll()
    BrowserPolicy.content.allowOriginForAll("*.gstatic.com");
    BrowserPolicy.content.allowOriginForAll('*.bootstrapcdn.com');
    var getSurveyQuestionByTitle = function(surveyTitle){
    	var answer = VH.SurveyAnswers.findOne({title : surveyTitle },{_id :1});
		if(typeof answer == "undefined" || !answer){
			console.log('could not find answer with title ' + surveyTitle);
			answer = false;
		}else{
			answer = answer._id;
		}
		return answer;
    }
	
	surveyTimeout = 20;
	// timeout in DAYS
	surveyExpires = 3;

	mInnerArray = function(location,fieldname,keyname,value){
		if(typeof location != "undefined" && typeof fieldname == "string" && typeof keyname != "undefined" && typeof value != "undefined"){
			var obj = {};
			if(typeof keyname != "object" && typeof value != "array" && typeof keyname != "array"){
				console.log('11:\t' + location + ':\t:' + fieldname + '.' + (location) + '.' + keyname );
				var location =  (location === 0 ? 0 : location + 0 );
				
				obj[fieldname + '.' + (location) + '.' + keyname] = value;
			}else{
				if(keyname.length === value.length){
					var r = {}
					console.log( 'line 29');
					console.log(typeof location + " : " + location);
					// this is for true values that may be rendered as 'true' instead of '1'
					location = (typeof location == "string" ? location :  location + 0 );
					console.log(' new location ' + location);
					// assuming location is alw ays a string.. this may not work as planned...
					keyname.filter(function(o,i){

						r[fieldname + '.' + (location) + '.' + o] = value[i];
					});
					return r;
				}else{
					console.log("Miss-matched keynames to filter...")
				}
			}
			if(obj){
				return obj;
			}
		}
		return false;
	}
	if(VH.Survey.findOne()){
		;
	}else{
		// add answers


		if(VH.SurveyAnswers.findOne()){
		// do nothing
		;
	}else{
		// insert base answers
		console.log("Inserting a few sample answers...")
		
		
		VH.SurveyAnswers.insert({  owner:1,
									"aType" : "scale", 
									
									    "aValues": [
								        {
								            "valueLabel": "None of the time",
								            "valueClass": "status",
								            "valueOrder": 1
								        },
								        {
								            "valueLabel": "Rarely",
								            "valueClass": "status",
								            "valueOrder": 2
								        },
								        {
								            "valueLabel": "Some of the time",
								            "valueClass": "status",
								            "valueOrder": 3
								        },
								        {
								            "valueLabel": "Often",
								            "valueClass": "status",
								            "valueOrder": 4
								        },
								        {
								            "valueLabel": "All of the time",
								            "valueClass": "status",
								            "valueOrder": 5
								        }
								    ],
									"isRadio" : true, 
									"mFirst" : 1, 
									"mLast" : 5, 
									"mSize" : 5,  
									"title" : "PHQ9-WEMWBS" }
								);
								
		VH.SurveyAnswers.insert({	owner: 1,
									"aType": "scale",
									  "aValues": [
								        {
								            "valueLabel": "Did not apply to me at all",
								            "valueClass": "status",
								            "valueOrder": 0
								        },
								        {
								            "valueLabel": "Applied to me to some degree, or some of the time",
								            "valueClass": "status",
								            "valueOrder": 1
								        },
								        {
								            "valueLabel": "Applied to me to a considerable degree, or a good part of time",
								            "valueClass": "status",
								            "valueOrder": 2
								        },
								        {
								            "valueLabel": "Applied to me very much, or most of the time",
								            "valueClass": "status",
								            "valueOrder": 3
								        }
								    ],
									"isRadio": true,
									"mFirst": 0,
									"mLast": 3,
									"mSize": 4,
									"title": "DASS"
								});
		VH.SurveyAnswers.insert({  		owner:1,
										"aType" : "scale", 
									   "aValues": [
									        {
									            "valueLabel": "Not at all",
									            "valueClass": "status",
									            "valueOrder": 0
									        },
									        {
									            "valueLabel": "Several days",
									            "valueClass": "status",
									            "valueOrder": 1
									        },
									        {
									            "valueLabel": "More than half the days",
									            "valueClass": "status",
									            "valueOrder": 2
									        },
									        {
									            "valueLabel": "Nearly every day",
									            "valueClass": "status",
									            "valueOrder": 3
									        }
									    ],
									    "isRadio": true,
									    "mFirst": 0,
									    "mLast": 3,
									    "mSize": 4,
										"title" : "GAD7" }
								);
		VH.SurveyAnswers.insert({	owner: 1,
									"aType": "scale",
									"aValues": [
										{
											"valueLabel": "Never",
											"valueClass": "status",
											"valueOrder": 0
										},
										{
											"valueLabel": "Almost Never",
											"valueClass": "status",
											"valueOrder": 1
										},
										{
											"valueLabel": "Sometimes",
											"valueClass": "status",
											"valueOrder": 2
										},
										{
											"valueLabel": "Fairly Often",
											"valueClass": "status",
											"valueOrder": 3
										},
										{
											"valueLabel": "Very Often",
											"valueClass": "status",
											"valueOrder": 4
										}
									],
									"isRadio": true,
									"mFirst": 0,
									"mLast": 4,
									"mSize": 5,
									"title": "PSS"
								});
								

		VH.SurveyAnswers.insert({      owner: 1,
									    "aType": "scale",
									    "aValues": [
									        {
									            "valueLabel": "None or a little of the time",
									            "valueClass": "status",
									            "valueOrder": 1
									        },
									        {
									            "valueLabel": "Some of the time",
									            "valueClass": "status",
									            "valueOrder": 2
									        },
									        {
									            "valueLabel": "Good part of the time",
									            "valueClass": "status",
									            "valueOrder": 3
									        },
									        {
									            "valueLabel": "Most or all of the time",
									            "valueClass": "status",
									            "valueOrder": 4
									        }
									    ],
									    "isRadio": true,
									    "mFirst": 1,
									    "mLast": 4,
									    "mSize": 4,
									    "title": "ZungAnxiety"}
								);
		VH.SurveyAnswers.insert({      owner: 1,
									    "aType": "scale",
									    "aValues": [
									        {
									            "valueLabel": "None or a little of the time",
									            "valueClass": "status",
									            "valueOrder": 4
									        },
									        {
									            "valueLabel": "Some of the time",
									            "valueClass": "status",
									            "valueOrder": 3
									        },
									        {
									            "valueLabel": "Good part of the time",
									            "valueClass": "status",
									            "valueOrder": 2
									        },
									        {
									            "valueLabel": "Most or all of the time",
									            "valueClass": "status",
									            "valueOrder": 1
									        }
									    ],
									    "isRadio": true,
									    "mFirst": 4,
									    "mLast": 1,
									    "mSize": 4,
									    "title": "ZungAnxiety Reverse" });
		}
								
	
	    var today = new Date();
		// insert base surveys without owners/answers
		console.log("Inserting base measures");
		var answer = getSurveyQuestionByTitle("PHQ9-WEMWBS");
		
		VH.Survey.insert(
			{"title" : "Personal Health Questionnaire", 
			"category" : "Depression", 
			"userType" : "user", 
			"issueDate" : today, 
			"issueInterval" : "biweekly", 
			"questions" : [ 
				{ "q" : "Over the last 2 weeks, how often have you been bothered by any of the following problems?", "labelStyle" : "left", "isPrompt" : true, "order" : 1 },
				{ "q" : "Little interest or pleasure in doing things", "labelStyle" : "row", "isPrompt" : false, "order" : 2, "answerId" : answer },
				{ "q" : "Feeling down, depressed, or hopeless", "answerId" : false, "labelStyle" : "row", "isPrompt" : false, "order" : 3, "answerId" : answer },
				{ "q" : "Trouble falling or staying asleep, or sleeping too much", "answerId" : "QNiMRKm3DFSpxRutd", "labelStyle" : "row", "isPrompt" : false, "order" : 4, "answerId" : answer }, 
				{ "q" : "Feeling tired or having little energy", "labelStyle" : "row", "isPrompt" : false, "order" : 5 }, { "q" : "Poor appetite or overeating",  "labelStyle" : "row", "isPrompt" : false, "order" : 6, "answerId" : answer }, 
				{ "q" : "Feeling bad about yourself—or that you are a failure or have let yourself or your family down", "labelStyle" : "row", "isPrompt" : false, "order" : 7,"answerId" : answer }, 
				{ "q" : "Trouble concentrating on things, such as reading the newspaper or watching television", "labelStyle" : "row", "isPrompt" : false, "order" : 8,"answerId" : answer }, 
				{ "q" : "Moving or speaking so slowly that other people could have noticed. Or the opposite—being so fidgety or restless that you have been moving around a lot more than usual", "labelStyle" : "row", "isPrompt" : false, "order" : 9,"answerId" : answer } ], 
			"organization" : "admin", "owner" : 1 }
		);
		// figure out ID's of base measures..
		answer = getSurveyQuestionByTitle("ZungAnxiety");
		
		var answerReverse = getSurveyQuestionByTitle("ZungAnxiety Reverse");

		VH.Survey.insert(
			{"title" : "Zung Self-rating Anxiety Scale", 
			"category" : "Anxiety", 
			"userType" : "user", 
			"issueDate" : today, 
			"issueInterval" : "weekly", 
			"questions" : [ 
				{ "q" : "Please read each one carefully and decide how much the statement describes how you have been feeling during the past week. Select the appropriate number for each statement.", "labelStyle" : "row", "isPrompt" : true, "order" : 1 }, 
				{ "q" : "I feel more nervous and anxious than usual.", "isPrompt" : false, "order" : 2, "answerId" : answer }, 
				{ "q" : "I feel afraid for no reason at all.", "isPrompt" : false, "order" : 3, "answerId" : answer}, 
				{ "q" : "I get upset easily or feel panicky.", "isPrompt" : false, "order" : 4,"answerId" : answer }, 
				{ "q" : "I feel like I'm falling apart and going to pieces.", "isPrompt" : false, "order" : 5,"answerId" : answer  }, 
				{ "q" : "I feel that everything is all right and nothing bad will happen.", "isPrompt" : false, "order" : 6 ,"answerId" : answerReverse  }, 
				{ "q" : "My arms and legs shake and tremble.", "isPrompt" : false, "order" : 7,"answerId" : answer }, 
				{ "q" : "I am bothered by headaches, neck and back pains.", "isPrompt" : false, "order" : 8 ,"answerId" : answer }, 
				{ "q" : "I feel weak and get tired easily.", "isPrompt" : false, "order" : 9,"answerId" : answer  }, 
				{ "q" : "I feel calm and can sit still easily.", "isPrompt" : false, "order" : 10,"answerId" : answerReverse  }, 
				{ "q" : "I can feel my heart beating fast.", "isPrompt" : false, "order" : 11,"answerId" : answer  }, 
				{ "q" : "I am bothered by dizzy spells.", "isPrompt" : false, "order" : 12,"answerId" : answer  }, 
				{ "q" : "I have fainting spells or feel faint.", "isPrompt" : false, "order" : 13,"answerId" : answer  }, 
				{ "q" : "I can breathe in and out easily.", "isPrompt" : false, "order" : 14,"answerId" : answerReverse  }, 
				{ "q" : "I get feelings of numbness and tingling in my fingers and toes.", "isPrompt" : false, "order" : 15, "answerId" : answer  }, 
				{ "q" : "I am bothered by stomachaches or indigestion.", "isPrompt" : false, "order" : 16, "answerId" : answer  }, 
				{ "q" : "I have to empty my bladder often.", "isPrompt" : false, "order" : 17, "answerId" : answer  }, 
				{ "q" : "My hands are usually dry and warm.", "isPrompt" : false, "order" : 18 ,"answerId" : answerReverse }, 
				{ "q" : "My face gets hot and blushes.", "isPrompt" : false, "order" : 19,"answerId" : answer  }, 
				{ "q" : "I fall asleep easily and get a good night's rest.", "isPrompt" : false, "order" : 20, "answerId" : answerReverse  }, 
				{ "q" : "I have nightmares.", "isPrompt" : false, "order" : 21 ,"answerId" : answer } ],
			"organization" : "admin", "owner" : 1 }
		);
		answer = getSurveyQuestionByTitle("DASS");
		
		VH.Survey.insert({
			"title" : "Depression Anxiety and Stress Scale", 
			"category" : "Anxiety and Stress", 
			"userType" : "user", 
			"issueDate" : today, 
			"issueInterval" : "weekly", 
			"questions" : [ 
				{ "q" : "Please read each statement and select an answer which indicates how much the statement applied to you over the past week. There are no right or wrong answers. Do not spend too much time on any statement.", "labelStyle" : "row", "isPrompt" : true, "order" : 1 }, 
				{ "q" : "I found myself getting upset by quite trivial things", "isPrompt" : false, "order" : 2 ,"answerId" : answer }, 
				{ "q" : "I found myself getting upset by quite trivial things", "isPrompt" : false, "order" : 3 ,"answerId" : answer }, 
				{ "q" : "I couldn't seem to experience any positive feeling at all", "isPrompt" : false, "order" : 4,"answerId" : answer }, 
				{ "q" : "I experienced breathing difficulty (e.g. excessively rapid breathing,\u0000 breathlessness in the absence of physical exertion)", "isPrompt" : false, "order" : 5,"answerId" : answer }, 
				{ "q" : "I just couldn't seem to get going", "isPrompt" : false, "order" : 6,"answerId" : answer  },
				{ "q" : "I tended to over-react to situations", "isPrompt" : false, "order" : 7,"answerId" : answer }, 
				{ "q" : "I had a feeling of shakiness (eg, legs going to give way)", "isPrompt" : false, "order" : 8,"answerId" : answer } ], 
			"organization" : "admin", "owner" : 1 }
		);
	}
	
});